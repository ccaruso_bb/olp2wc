angular.module("app")
    .controller("printPayCtrl", ["$scope", "$rootScope", "idealsApi", function($scope, $rootScope, idealsApi) {

        try {
// get cust_information api method response and bind data  on html table

            $rootScope.message = "";
            var listCards = JSON.parse(sessionStorage["cust_info"] || "{}");
            var json = listCards.data.v1.customerInfo;
            $scope.customerID = json.customerID;
            $scope.Name = json.firstName + " " + json.lastName;
            $scope.address = json.address1 + " " + json.address2 + "The City," + json.city + " " + json.state + " " + json.zip;

//var payJson = paymentinfo.data.v1.
            var count = 0;
            $scope.payment = JSON.parse(sessionStorage["paymentAmt"] || "{}");

            for (var item = 0; item < $scope.payment.length; item++) {
                count = count + parseFloat($scope.payment[item].amountOfPayment);
            };

            $scope.total = "$" + (count).toFixed(2);
            var paymentinfo = JSON.parse(sessionStorage["pay_stored_card"] || "{}");
            $scope.approvalCode = paymentinfo.data.v1.approvalCode;
            $scope.paidAmount = $scope.total;
            $scope.receiptNumber = paymentinfo.data.v1.receiptNumber;

            // print page view
            $scope.printDirective = function(printSectionId) {
                var innerContents = document.getElementById(printSectionId).innerHTML;
                var popupWinindow = window.open("", "_blank", "width=1000,height=1000,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no");
                popupWinindow.document.open();
                popupWinindow.document.write('<html><head><style>table.right_align_3 th {text-align: left;}</style><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + innerContents + '</body></html>');
                popupWinindow.document.close();

            };

            $scope.localDateTime = function(date) {
                var date = new Date(date);
                return date;
            }

        } catch (e) {
            console.log(e);
        }

    }]);
