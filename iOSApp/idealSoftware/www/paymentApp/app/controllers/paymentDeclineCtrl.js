﻿angular.module("app")
    .controller("paymentDeclineCtrl", ["$scope", "$rootScope", "$location", "idealsApi", function($scope, $rootScope, $location, idealsApi) {

        try {
            // redirect to checkout page
            $scope.clickCheckout = function() {
                $rootScope.message = "";
                $location.path('/checkOut');

            };
        } catch (e) {
            console.log(e);
        }

    }]);