angular.module("app")
    .controller("makePayCtrl", ["$scope", "$rootScope", "idealsApi", function($scope, $rootScope, idealsApi) {

        try {
            $rootScope.message = "";
            // get cust_information api method response and bind data  on html table
            var listCards = JSON.parse(sessionStorage["cust_info"] || "{}");
            var json = listCards.data.v1.customerInfo;
            $scope.customerID = json.customerID;
            $scope.Name = json.firstName + " " + json.lastName;
            if(json.address2 == "")
            {
            $scope.address = json.address1 + ", " + json.address2 + " "+ json.city + ", " + json.state + " " + json.zip;
            }
            else
            {
            $scope.address = json.address1 + ", " + json.address2 + ", "+ json.city + ", " + json.state + " " + json.zip;
            }

            var count = 0;
            $scope.payment = JSON.parse(sessionStorage["paymentAmt"] || '{}');

            for (var item = 0; item < $scope.payment.length; item++) {
                count = count + parseFloat($scope.payment[item].amountOfPayment);
            };

            $scope.total = "$" + (count).toFixed(2);
            var paymentinfo = JSON.parse(sessionStorage["pay_stored_card"] || "{}");

            $scope.approvalCode = paymentinfo.data.v1.approvalCode;
            $scope.paidAmount = $scope.total;
            $scope.receiptNumber = paymentinfo.data.v1.receiptNumber;

            $scope.localDateTime = function(date) {
                var date = new Date(date);
                return date;
            }

        } catch (e) {
            console.log(e);
        }

    }]);
