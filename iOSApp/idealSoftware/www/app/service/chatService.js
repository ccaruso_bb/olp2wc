
// call all api methods using faye server
angular.module("app")
.service("chatService", ["$rootScope", "$cookieStore", "$location", "$timeout", "SharedService", "configDetailsProvider", function($rootScope, $cookieStore, $location, $timeout, SharedService, configDetailsProvider) {
                try
                {
                      function fayClass(ID) {
                       var responseChannelPrefix;
                       var clientID;
                       var data;
                       var evl;
                       var client;
                       var responseChannel;
                       var isSubscribed = false;
                       var handshake = "/meta/handshake";
                       var connect = "/meta/connect";
                       var subscribe = "/meta/subscribe";
                       var validToken;
                       var Logger;
                       var time_last;
                       var AuthenticateRequest;
                       var apiToken;
                       var apiKey;
                       var apiStore;
                       var groupId;
                       var location_url;
                       var firstConnection = true;
                       var timeout;
                       var siteId;
                       var waitTime = 120000;
                       var waitTimeReq = "";
                       var storeToken;

                       groupId = configDetailsProvider.apiConnect.group;
                       apiStore = configDetailsProvider.apiConnect.api1;
                       console.log("cookies ..........." + groupId + "............" + apiStore);
                         
                       if (sessionStorage.getItem("siteID") == null || sessionStorage.getItem("siteID") == "") {
                       siteId = ID;
                       } else {
                       siteId = ID;
                       }
                       this.locationID=ID;

                       responseChannelPrefix = "/" + apiStore + "/" + groupId + "/" + siteId;
                       
                       // Connect To Faye
                       var client_url = configDetailsProvider.apiConnect.client_url;
                       var start = new Date().getTime();
                       
                       
                       client = new Faye.Client(client_url, {
                                                timeout: 120
                                                });
                         

                         
                       client.connect();
                         
                    $rootScope.client1 = client;//FF
                         
                       client.disable("WebSocket");
                       
                       Logger = {
                       incoming: function(message, callback) {
                       
                       console.log(" incoming: ", message);
                       if (message.channel == handshake && message.successful) {
                       var obj = JSON.parse(JSON.stringify(message));
                       clientID = (obj.clientId);
                       responseChannel = "/" + apiStore + "/" + groupId + "/" + clientID + "/response";
                       
                       console.log("for subscribe...." + responseChannel);
                       $rootScope.messageReceived = {
                       data: {
                       message: "connect"
                       }
                       };
                       $rootScope.$apply();
                       if (!isSubscribed) {
                       SubscribeIt(responseChannel);
                       }
                       }
                       return callback(message);
                       },
                       outgoing: function(message, callback) {
                       if (message) {
                       
                       console.log(" outgoing: ", message);
                       try {
                       evl = "";
                       message.ext = "";
                       var jsonString = JSON.stringify(message);
                       var json = message;
                       apiToken = configDetailsProvider.apiConnect.token;
                       apiStore = configDetailsProvider.apiConnect.api1;
                       if (message.channel == subscribe) {
                       
                       var salt = random128();
                       evl = {
                       "api": apiStore,
                       "token": apiToken,
                       "salt": Base64.encode(salt),
                       "signature": createSignature(salt, jsonString),
                       "message": subscribe,
                       "data": Base64.encode(jsonString)
                       };
                       message.ext = evl;
                       return callback(message)
                       };
                       
                       if (json.data != "undefined" && json.data != null) {
                       var salt = random128();
                       var message1;
                       if ((json.data != null) && (json.data.v1 != null)) {
                       message1 = json.data.message;
                       } else {
                       console.log("[VR][API]Packet missing v1 section or v1.message element. \r\n" + json.data);
                       }
                       message.ext = "";
                       
                       evl = {
                       "api": apiStore,
                       "token": apiToken,
                       "salt": Base64.encode(salt),
                       "signature": createSignature(salt, jsonString),
                       "message": message1,
                       "data": Base64.encode(jsonString)
                       };
                       
                       message.ext = evl;
                       message.data = json.data;
                       };
                       } catch (err) {
                       console.log(err.message);
                       }
                       }
                       
                       callback(message);
                       }
                       }
                       
                       // call extension
                       client.addExtension(Logger);
                       console.log(Logger);
                         
                       function cancelTimer() {
                       console.log("cancel timeout");
                       $timeout.cancel(timeout);
                       };
                       
                       function stopLoader() {
                       $rootScope.ShowLoader = false;
                       console.log("The server cannot or will not process the request");
                       $rootScope.apiErrorMessage = "The server cannot or will not process the request";
                       };
                       
                       function startTimer() {
                       timeout = $timeout(function() {
                          console.log("finish timeout");
                          $rootScope.ShowLoader = false;
                          console.log("The server cannot or will not process the request");
                          $rootScope.apiErrorMessage = "The server cannot or will not process the request";
                          
                          }, waitTime);
                       };
                       
                       //Subscribe here
                       function SubscribeIt(rc) {
                       var subscription = client.subscribe(rc, function(msg) {
                               console.log("subscribed data " + JSON.stringify(msg));
                               $rootScope.messageReceived = msg;
                               $rootScope.$apply();
                               
                               if (msg.data.message == "customer_hello" && msg.data.v1.success == true) {
                      
                               sessionStorage.validToken = msg.data.v1.token;
                               storeToken=msg.data.v1.token;;
                               
                               }

                               }).then(function(msg) {
                                       console.log("subscription successful!!");
                                       console.log("subscribed");
                                       isSubscribed = true;
                                       customer_helloReq();

                                       }, function(error) {
                                       console.log("Error subscribing: " + error.message);
                                       });
                       };
                       
                         
                    // Authenticate Request
                     function customer_helloReq() {
//                         alert("Hello Customer Req");
                     var data = customer_hello();
                   
                     var publication = client.publish(responseChannelPrefix, data).then(function() {
                            console.log("Successfully published!");
                            
                            }, function(error) {
                            console.log("Error publishing: " + error.message);
                            
                            });
                     }
                       
                       //site_information
                         function customer_hello() {
                         var messageid = createGuid();
                         var currenttimestamp = CreateDate();
                         var data = {
                         "v1": {
                         "returnChannel": responseChannel,
                         "messageID": messageid,
                         "siteID": siteId,
                         "pushToken": sessionStorage.getItem("FCMToken"),
                         
                         "deviceID": sessionStorage.getItem("DeviceId"),
                         "platform": sessionStorage.getItem("DevicePlatform"),
                         "phone": "",
                         "email": "",
                         "currentTimeStamp": currenttimestamp
                         },
                         "message": "customer_hello"
                         };
                         return data;
                         };


                         this.customer_messageReq = function(msg) {
                         
                         data = customer_message(msg);
                        
                         var publication = client.publish(responseChannelPrefix, data).then(function() {
                                console.log("Successfully published!");
                                }, function(error) {
                                });
                         }
                         
                         //site_information
                         function customer_message(msg) {
                       
                         var messageid = createGuid();
                         var currenttimestamp = CreateDate();
                         var data = {
                         "v1": {
                         "messageID": messageid,
                         "siteID": siteId,
                         "currentTimeStamp": currenttimestamp,
                         "ackChannel": responseChannel,
                         "token":storeToken,
                         "messageBody": msg,
                         "messageType": ""
                         },
                         "message": "customer_message"
                         };
                         return data;
                         };

                       
                       // signature
                       function createSignature(salt, json) {
                       // api_key
                       var api_key = configDetailsProvider.apiConnect.key;
                       var signature = (SHA256(SHA256(SHA256(utf8.encode(json)) + utf8.encode(api_key)) + (salt)));
                       return signature;
                       };
                       
                       
                       // guid
                       function createGuid() {
                       
                       function _p8(s) {
                       var p = (Math.random().toString(16) + "000000000").substr(2, 8);
                       return s ? "-" + p.substr(0, 4) + "-" + p.substr(4, 4) : p;
                       }
                       return _p8() + _p8(true) + _p8(true) + _p8();
                       };
                       
                       // currentdatetime
                       function CreateDate() {
                       var currentdate = new Date();
                       var currentDateTime = currentdate.toISOString();
                       //var currentDateTime = (currentdate.toLocaleString()).replace(',', '');
                       return currentDateTime;
                       };
                       
                       // random128
                       function random128() {
                       
                       var result = "";
                       for (var i = 0; i < 8; i++)
                       result += String.fromCharCode(Math.random() * 0x10000);
                       //var salt = Base64.encode(result);
                       return result;
                       };
                         
                       }
                         
                         $rootScope.myList =[];
//                       var objJsonStoreData = JSON.parse($rootScope.jsonStoreData);
                         var objJsonStoreData = JSON.parse(sessionStorage["jsonStoreData"]);
                         for(var i = 0; i < objJsonStoreData.length; i++){
                         var storeNumber = parseInt(objJsonStoreData[i].number);
                         $rootScope.myList.push(new fayClass(storeNumber));
                         
                         }
                       
//                       $rootScope.myList =[];
//                       $rootScope.myList.push(new fayClass(1));
//                       $rootScope.myList.push(new fayClass(123));
//                       $rootScope.myList.push(new fayClass(234));
//                       $rootScope.myList.push(new fayClass(2));
                         
                         
                       } catch (e) {
                       // errorMessage redirect to error page.
                       console.log(e);
                       $location.path("/error").search("ref", e.message);
                       $rootScope.Loaded = true;
                       }
                       
                       }]);
