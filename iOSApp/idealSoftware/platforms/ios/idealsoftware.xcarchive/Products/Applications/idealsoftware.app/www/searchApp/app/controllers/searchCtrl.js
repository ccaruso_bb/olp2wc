angular.module("app")
    .controller("searchCtrl", ["$scope", "$rootScope", "$state", "idealsApi", "$cookieStore", "$compile","filterItem", function($scope, $rootScope, $state, idealsApi, $cookieStore, $compile,filterItem) {
        try {
            // check api response
            var list_modelsResponse = JSON.parse(sessionStorage["list_models"] || "{}");
            var json1 = list_modelsResponse;
            var modelsList = json1.data.v1.models;
            $scope.inventoryList = json1.data.v1.models;
            var inventroy = "";
            $scope.Brands = [];
            $scope.WeeklyRates = [];
            $scope.MonthlyRates = [];
            $scope.Price = [];
            $scope.ModelNumber = [];
            $scope.Description = [];

            // bind categories of item..................................................
            for (var item = 0; item < modelsList.length; item++) {

                var productBrands = {
                    brand: modelsList[item].brand
                };

                var productWeeklyRate = {
                    weeklyRate: modelsList[item].weeklyRate
                };

                var productMonthlyRate = {
                    monthlyRate: modelsList[item].monthlyRate
                };

                var productPrice = {
                    price: modelsList[item].price
                };

                var productModelNumber = {
                    modelNumber: modelsList[item].modelNumber
                };

                var productDescription = {
                    description: modelsList[item].description
                };

                $scope.Brands.push(productBrands);
                $scope.WeeklyRates.push(productWeeklyRate);
                $scope.MonthlyRates.push(productMonthlyRate);
                $scope.Price.push(productPrice);
                $scope.ModelNumber.push(productModelNumber);
                $scope.Description.push(productDescription);

            };


            // reset the categories
            $scope.reset = function() {
                $scope.selectedProduct = "";
                $scope.selectedBrand = "";
                $scope.Modl.selectedModelNumber = "";
                $scope.selectedPrice = "";
                $scope.selectedWeeklyRate = "";
                $scope.selectedMonthlyRate = "";
            }

            // click on refine search to call inventory details
            $scope.refineSearch = function(selectedModelNumber) {
                if ($scope.searchForm.$valid) {

                    for (var item = 0; item < modelsList.length; item++) {

                        if (modelsList[item].modelNumber == selectedModelNumber.modelNumber) {

                            idealsApi.list_inventoryReq(modelsList[item].id);
                            break;

                        }
                    }
                }
            }

            // this is the solution to keep the drop down list selection after webpage refresh
            for (var index = 0; index < $scope.ModelNumber.length; index++) { //Loop through each array element

                if ($scope.ModelNumber[index].modelNumber === sessionStorage.getItem("selectedModl")) { //Check if any of the 'year' value in the array match the value stored in sessionStorage
                    $scope.Modl = {}; //reset the selected scope variable
                    $scope.Modl.selectedModelNumber = $scope.ModelNumber[index]; //Assign the array element with the matching year, as the selected scope element
                }

            }

            $scope.$watch("Modl.selectedModelNumber.modelNumber", function(Modl) { //Set watch on array element 'year' for changes/selection
                sessionStorage.setItem("selectedModl", Modl); //When an element is selected from the drop down list, its value is stored in sessionStorage


            });


            // response of list_inventory method

//commented by F 15-12-18 --- it may be require...

//            if (sessionStorage["list_inventory"]) {
//                $scope.inventoryList = "";
//                var data = "";
//                var list_inventoryResponse = JSON.parse(sessionStorage["list_inventory"] || "{}");
//                var json2 = list_inventoryResponse;
//
//                if(json2!="")
//                {
//
//                                var data = json2.data.v1.items;
//                                $scope.data = (JSON.parse(sessionStorage["cartInfo"] || "{}"));
//                                console.log("data >>>>>>>>>>>" + data);
//                                alert("data" + data);
//                                for (var j = 0; j <= (data).length - 1; j++) {
//                                    (data[j])["ItemSelected"] = "N";
//                                    json2.data.v1.items[j] = data[j];
//                                }
//
//                                if (JSON.parse(sessionStorage["cartInfo"] || "{}")) {
//                                    var selectedItem = JSON.parse(sessionStorage["cartInfo"] || "{}");
//                                    for (var itemS = 0; itemS <= (selectedItem).length - 1; itemS++) {
//                                        for (var dataItem = 0; dataItem <= (data).length - 1; dataItem++) {
//                                            if (data[dataItem].id == selectedItem[itemS].id) {
//                                                (data[dataItem])["ItemSelected"] = "Y";
//                                                json2.data.v1.items[dataItem] = data[dataItem];
//                                            } else {
//                                                if ((data[dataItem])["ItemSelected"] != "Y") {
//                                                    (data[dataItem])["ItemSelected"] = "N";
//                                                    json2.data.v1.items[dataItem] = data[dataItem];
//                                                }
//                                            }
//                                            console.log(data[dataItem]);
//                                            console.log("data 2 >>>>>>>>>>>>>>>>>>" + data[dataItem]);
//                                        }
//                                    }
//                                }
//
//
//                                if (data[0].ItemSelected) {
//
//                                } else {
//                                    for (var item = 0; item <= (data).length - 1; item++) {
//                                        (data[item])["ItemSelected"] = "N";
//                                        json2.data.v1.items[item] = data[item];
//                                    }
//
//                                    $scope.inventoryList = json2.data.v1.items;
//                                    inventroy = json2;
//                                    console.log("inventoryList---------" + $scope.inventoryList);
//                                }
//
//
//                                $scope.inventoryList = json2.data.v1.items;
//                                inventroy = json2;
//                                var selectItemDetails = {};
//                                var itemsArray = [];
//                                $scope.isDisabled = false;
//                }
//                else
//                {
//
//                    var list_modelsResponse = JSON.parse(sessionStorage["list_models"] || "{}");
//                            var json1 = list_modelsResponse;
//                            var modelsList = json1.data.v1.models;
//                            $scope.inventoryList = json1.data.v1.models;
//                }
//
//            }

// End--------------15-12-18

            // click on new item to select all new items only
            $scope.ClickNewItem = function($event) {
                $scope.inventoryList = [];
                if (sessionStorage["list_inventory"] != "") {
                    var list_inventoryResponse = JSON.parse(sessionStorage["list_inventory"] || "{}");
                    var json2 = list_inventoryResponse;

                    var inventoryList = json2.data.v1.items;
                    if ($event == true) {
                        for (var item = 0; item <= (inventoryList).length - 1; item++) {
                            if (inventoryList[item].isNew == true) {
                                $scope.inventoryList.push(inventoryList[item]);
                            }
                        }
                    } else {
                        for (var item = 0; item <= (inventoryList).length - 1; item++) {
                            $scope.inventoryList.push(inventoryList[item]);
                        }
                    }
                } else {}
            }

            // range select for item
            $scope.selectRange = function(selectedRange) {
                if (selectedRange == filterItem.cashPriceLH || selectedRange == 0) {
                    $scope.filter = filterItem.fPriceLH;
                } else if (selectedRange == filterItem.cashPriceHL || selectedRange == 1) {
                    $scope.filter = filterItem.fPriceHL;
                } else if (selectedRange == filterItem.weeklyRateLH || selectedRange == 2) {
                    $scope.filter = filterItem.fWeeklyLH;
                } else if (selectedRange == filterItem.weeklyRateHL || selectedRange == 3) {
                    $scope.filter = filterItem.fWeeklyHL;
                } else if (selectedRange == filterItem.monthlyRateLH || selectedRange == 4) {
                    $scope.filter = filterItem.fmonthlyLH;
                } else if (selectedRange == filterItem.monthlyRateHL || selectedRange == 5) {
                    $scope.filter = filterItem.fmonthlyHL;
                }
            };

            //select the item and update the cart
            $scope.selectitem = function(selectedData, index) {

                for (var item = 0; item <= $scope.inventoryList.length; item++) {
                    if ($scope.inventoryList[item] == selectedData) {
                        $scope.inventoryList[item].ItemSelected = "Y";
                    }
                }

                selectItemDetails = {};
                console.log(selectedData);
                selectItemDetails = selectedData;

                var selModelNumber = sessionStorage.getItem("selectedModl");
                var selProductName = sessionStorage.getItem("selectedProdN");
                var prodID = getProdID(selProductName);
                var modelID = getModelID(selModelNumber);

                selectItemDetails["pcid"] = (prodID);
                //selectItemDetails["modid"] = (modelID);
                selectItemDetails["modid"] = selectedData.id;
                selectItemDetails["quantity"] = 1;

                $scope.cardUpdate(selectItemDetails);
                sessionStorage["list_inventory"] = "";
                sessionStorage["list_inventory"] = JSON.stringify(inventroy);
                var json = JSON.parse(sessionStorage["list_inventory"] || "{}");
                console.log(json);
            };


            function getModelID(selModelNumber) {
                var modelID = "";
                var list_modelsResponse = JSON.parse(sessionStorage["list_models"] || "{}");
                var json1 = list_modelsResponse;
                var modelsList = json1.data.v1.models;
                for (var item = 0; item <= modelsList.length - 1; item++) {
                    if (modelsList[item].modelNumber == selModelNumber) {
                        modelID = modelsList[item].id
                        break;
                    }
                };
                return modelID;
            };


            function getProdID(selProductName) {
                var ID = "";
                $scope.list_categories = JSON.parse(sessionStorage["list_categories"] || "{}");
                var json = $scope.list_categories;
                var department = json.data.v1.departments;
                for (var deptL = 0; deptL <= department.length - 1; deptL++) {

                    for (var prodL = 0; prodL <= department[deptL].productCodes.length - 1; prodL++) {
                        if (department[deptL].productCodes[prodL].description == selProductName) {
                            ID = department[deptL].productCodes[prodL].id
                            break;
                        }
                    }
                }
                return ID;
            };


        } catch (e) {
            console.log(e);
        }
    }]);
