angular.module("sharedService", ["ngCookies"])
    .config(function($provide) {
        $provide.provider('configDetailsProvider', function() {
            this.$get = function($http) {
                var configDetails = "";
                var json = sessionStorage.getItem('config');
                if (json != null) {
                    var json = JSON.parse(sessionStorage["config"] || "{}");
                    configDetails = {
                        group: json[0].group,
                         token: json[0].token,
                         key: json[0].apiKey,
                         url: json[0].url,
                         color: json[0].color,
                         client_url: json[0].client_url,
                         image: json[0].image,
                         flag: json[0].flag,
                         IsShopShow: json[0].IsShopShow,   // for hide the shop icon from home
                         ChatName: json[0].ChatName,
                         processor_type: json[0].processor_type,
                         "api": "vr_store",
                         "api1": "messaging_api",
                         "report_url": "https://test.idealss.net/report_a_problem",
                         "location_url": json[0].locationUrl,
                         "tac_url": json[0].tacUrl,
                         "location_id": parseInt(sessionStorage.getItem("storeNumber")),
                         "title": "Chetu, Inc.",
                         "env": "development",
                         "payeezy_key": "js-6125e57ce5c46e10087a545b9e9d7354c23e1a1670d9e9c7",
                         "api_key": "y6pWAJNyJyjGv66IsVuWnklkKUPFbb0a",
                         "ta_token": "NOIW"
                     }
                };
                return {
                    apiConnect: configDetails
                };
            }
        });
    })
    .service("SharedService", function($cookieStore, $timeout, $rootScope, $location, $http,$q) {
        try {
            //var sharedService;
            var sharedClient;
            var dataValue = {};
            var api;

            this.setData = function(data) {
                window._data = data;
                // Save data to the current local store
                localStorage.setItem("storeNumber", data);
                /* method code... */
            };

            this.getData = function() {
                //return window._data;
                return localStorage.getItem("storeNumber");
            };

//             this.testalert(){
//             alert("Hello from test");
//             }
             
            this.writePayezzyCookie = function() {
                var auth = {
                    "payeezy_key": "js-6125e57ce5c46e10087a545b9e9d7354c23e1a1670d9e9c7",
                    "api_key": "y6pWAJNyJyjGv66IsVuWnklkKUPFbb0a",
                    "ta_token": "NOIW"
                };
                $cookieStore.put("payeezy_auth", auth);
            }


                        this.setPhoneNumber=function(data)
                        {
                        window._data = data;
                        $rootScope.condition=false;
                        sessionStorage.setItem("phoneNumber", data);
                        }

                        this.getPhoneNumber = function() {
                            return sessionStorage.getItem("phoneNumber");

                        };

                        this.setEmailId=function(data)
                        {
                        window._data = data;
                        // Save data to the current local store
                        sessionStorage.setItem("EmailId", data);
                        }

                        this.getEmailId = function() {
                            return sessionStorage.getItem("EmailId");
                        };

                        this.removeSession=function()
                                    {
                                    //remove sessions
                                    sessionStorage.setItem("selectedDept", "");
                                    sessionStorage.setItem("selectedProdN", "");
                                    sessionStorage.setItem("cartInfo", "");
                                    sessionStorage.setItem("cust_info", "");
                                    sessionStorage.setItem("collect_deposit", "");
                                    sessionStorage.setItem("list_inventory", "");
                                    sessionStorage.setItem("list_models", "");
                                    sessionStorage.setItem("list_categories", "");
                                    sessionStorage.setItem("selectedModl", "");
             };



        } catch (e) {
            console.log(e);
        }
    });
