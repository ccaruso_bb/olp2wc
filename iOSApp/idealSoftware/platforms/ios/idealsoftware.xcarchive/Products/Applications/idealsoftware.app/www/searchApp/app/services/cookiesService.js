﻿angular.module("app")
    .service("Apicookies", ["$rootScope", "$cookieStore", "$location", function($rootScope, $cookieStore, $location) {
        try {

            this.writeApiCookie = function() {
                var report_url = "";

                if (sessionStorage.report_url) {
                    report_url = JSON.parse(sessionStorage["report_url"] || "{}");
                } else {
                    report_url = "";
                }
                var api = {
                    "token": "DFCA5E027D98B3C9671EF2E1189D2A4C19EB202969226396758FF9923947F696",
                    "key": "EC90D0E66B11CB53B43565894DBBC49592EA34ED57D6907001131318AB2EA0EEA1DE4D1A3E428F9424C294113336050954E52F50970F9D007A565B4D7B5AFAAC",
                    "group": "241",
                    "api": "vr_store",
                    "apiCrm": "crm_orders",
                    "client_url": "https://pubsub-test.idealss.net/faye",
                    "report_url": "https://test.idealss.net/report_a_problem",
                    "location_url": "chetu1",
                    "tac_url": "https://static.test.idealss.net",
                    "location_id": 1,
                    "title": "Chetu, Inc.",
                    "env": "development"
                };

                ($cookieStore.put("api_connect", api));
            };


            this.writePayezzyCookie = function() {
                var auth = {
                    "payeezy_key": "js-6125e57ce5c46e10087a545b9e9d7354c23e1a1670d9e9c7",
                    "api_key": "y6pWAJNyJyjGv66IsVuWnklkKUPFbb0a",
                    "ta_token": "NOIW"
                };
                $cookieStore.put("payeezy_auth", auth);
            }
            //function to remove cookie
            this.removeCookie = function() {
                $cookieStore.remove("api_connect");
                $cookieStore.remove("payeezy_auth");
            };
        } catch (e) {
            console.log(e);
        }
    }]);