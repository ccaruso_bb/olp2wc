// call all api methods using faye server
angular.module("app")
    .service("idealsApi", ["$rootScope", "$cookieStore", "$location", "$timeout", "SharedService", "configDetailsProvider", function($rootScope, $cookieStore, $location, $timeout, SharedService, configDetailsProvider) {
        var responseChannelPrefix;
        var clientID;
        var data;
        var evl;
        var client;
        var responseChannel;
        var isSubscribed = false;
        var handshake = "/meta/handshake";
        var connect = "/meta/connect";
        var subscribe = "/meta/subscribe";
        var validToken;
        var Logger;
        var time_last;
        var AuthenticateRequest;
        var apiToken;
        var apiKey;
        var apiStore;
        var groupId;
        var location_url;
        var firstConnection = true;
        var timeout;
        var siteId;
        var waitTime = 120000;
        var waitTimeReq = "";
        try {

            // cal and read api_cookies
            // SharedService.writeApiCookie();
            groupId = configDetailsProvider.apiConnect.group;
            apiStore = configDetailsProvider.apiConnect.api;
            console.log("cookies ..........." + groupId + "............" + apiStore);


            if (sessionStorage.getItem("siteID") == null || sessionStorage.getItem("siteID") == "") {
                siteId = parseInt(SharedService.getData());
            } else {
                siteId = parseInt(sessionStorage.getItem("siteID"));
            }
            responseChannelPrefix = "/" + apiStore + "/" + groupId + "/" + siteId;

            // Connect To Faye
            var client_url = configDetailsProvider.apiConnect.client_url;
            var start = new Date().getTime();


            client = new Faye.Client(client_url, {
                timeout: 120
            });
            client.connect();
            client.disable("WebSocket");

            Logger = {
                incoming: function(message, callback) {

                    console.log(" incoming: ", message);
                    if (message.channel == handshake && message.successful) {
                        var obj = JSON.parse(JSON.stringify(message));
                        clientID = (obj.clientId);
                        responseChannel = "/" + apiStore + "/" + groupId + "/" + clientID + "/response";

                        console.log("for subscribe...." + responseChannel);
                        $rootScope.messageReceived = {
                            data: {
                                message: "connect"
                            }
                        };
                        $rootScope.$apply();
                        if (!isSubscribed) {
                            SubscribeIt(responseChannel);
                        }
                    }
                    return callback(message);
                },
                outgoing: function(message, callback) {
                    if (message) {

                        console.log(" outgoing: ", message);
                        try {
                            evl = "";
                            message.ext = "";
                            var jsonString = JSON.stringify(message);
                            var json = message;
                            apiToken = configDetailsProvider.apiConnect.token;
                            apiStore = configDetailsProvider.apiConnect.api;
                            if (message.channel == subscribe) {

                                var salt = random128();
                                evl = {
                                    "api": apiStore,
                                    "token": apiToken,
                                    "salt": Base64.encode(salt),
                                    "signature": createSignature(salt, jsonString),
                                    "message": subscribe,
                                    "data": Base64.encode(jsonString)
                                };
                                message.ext = evl;
                                return callback(message)
                            };

                            if (json.data != "undefined" && json.data != null) {
                                var salt = random128();
                                var message1;
                                if ((json.data != null) && (json.data.v1 != null)) {
                                    message1 = json.data.message;
                                } else {
                                    console.log("[VR][API]Packet missing v1 section or v1.message element. \r\n" + json.data);
                                }
                                message.ext = "";

                                evl = {
                                    "api": apiStore,
                                    "token": apiToken,
                                    "salt": Base64.encode(salt),
                                    "signature": createSignature(salt, jsonString),
                                    "message": message1,
                                    "data": Base64.encode(jsonString)
                                };

                                message.ext = evl;
                                message.data = json.data;
                            };
                        } catch (err) {
                            console.log(err.message);
                        }
                    }

                    callback(message);
                }
            }


            // call extension
            client.addExtension(Logger);
            console.log(Logger);

            function cancelTimer() {
                console.log("cancel timeout");
                $timeout.cancel(timeout);
//                alert("canceled timeout");//F
            };

            function stopLoader() {
                $rootScope.ShowLoader = false;
                console.log("The server cannot or will not process the request");
                $rootScope.apiErrorMessage = "The server cannot or will not process the request";
            };

            function startTimer() {

                timeout = $timeout(function() {
//                 alert("startTimer"); //F
                    console.log("finish timeout");
                    $rootScope.ShowLoader = false;
                    console.log("The server cannot or will not process the request");
                    $rootScope.apiErrorMessage = "The server cannot or will not process the request";

                }, waitTime);
            };


            //Subscribe here
            function SubscribeIt(rc) {
                var subscription = client.subscribe(rc, function(msg) {
                    console.log("subscribed data " + JSON.stringify(msg));
                    $rootScope.messageReceived = msg;
                    $rootScope.$apply();

                    //clear timer
                    if (msg.data.message == waitTimeReq) {
                        cancelTimer();
                    }

                    //check store_information
                    if (sessionStorage.checkStoreInformation == null) {
                        site_informationReq();

//site_informationReq
                    }


                    if (msg.data.message == "authenticate" && msg.data.v1.errorDescription == "Successful") {

                        if (sessionStorage.getItem("key") === "Payments") {
                            sessionStorage.loggedUser = "valid";
                            sessionStorage.validToken = msg.data.v1.token;
                            // ListAccount Request
                            var data = listAccountsReq(msg);
                            var publication = client.publish(responseChannelPrefix, data).then(function() {
                                console.log("Successfully published!");
//                                waitTimeReq = "list_accounts"; //21-09-2018
//                                startTimer();
                            }, function(error) {
                                console.log("Error publishing: " + error.message);
                                stopLoader();
                            });

                        }
                    }

                    if (msg.data.message == "pay_stored_card" && msg.data.v1.errorDescription == "Successful") {
                        var data = customer_information(sessionStorage.customerID);
                        var publication = client.publish(responseChannelPrefix, data).then(function() {
                            console.log("Successfully published!");
                        }, function(error) {
                            console.log("Error publishing: " + error.message);
                        });
                    };

                }).then(function(msg) {
                    console.log("subscription successful!!");
                    console.log("subscribed");
                    isSubscribed = true;
                    site_informationReq();



                    if (sessionStorage.getItem("key") == "Payments") {
                        if ((sessionStorage.getItem("userName") != null || sessionStorage.getItem("userName") != "") && (sessionStorage.getItem("passWord") != null || sessionStorage.getItem("passWord") != "")) {
                            var username = sessionStorage.getItem("userName");
                            var password = sessionStorage.getItem("passWord");
                            authenticateReq(username, password);

                        }
                    }
                }, function(error) {
                    console.log("Error subscribing: " + error.message);
                });
            };

            //Client disconnect
            this.clientDisconnect = function() {
                client.unsubscribe();
                client.disconnect();
            }

            this.list_stored_cardsReq = function() {
                var data = list_stored_cards(sessionStorage.customerID);
                var publication = client.publish(responseChannelPrefix, data).then(function() {
                    console.log("Successfully published!");
                }, function(error) {
                    console.log("Error publishing: " + error.message);
                });
            };


            this.pay_stored_cardReq = function(Cardtoken, expDate, first6, last4, cardHolder, cardType, payment, total, processType) {
                var data = pay_stored_card(Cardtoken, expDate, first6, last4, cardHolder, cardType, payment, total, processType, sessionStorage.customerID);
                var publication = client.publish(responseChannelPrefix, data).then(function() {
                    console.log("Successfully published!");
                    waitTimeReq = "pay_stored_card";
                    startTimer();
                }, function(error) {
                    console.log("Error publishing: " + error.message);
                    stopLoader();
                });
            }
            this.site_informationReq = function(sId) {
                siteId = sId;
                responseChannelPrefix = "/" + apiStore + "/" + groupId + "/" + siteId;
                console.log("channer info==" + responseChannelPrefix);
                var data = site_information(siteId);
                var publication = client.publish(responseChannelPrefix, data).then(function() {
                    console.log("Successfully published!");
                    //waitTimeReq = "site_information";
                    //startTimer();
                }, function(error) {
                    console.log("Error publishing: " + error.message);
                });

            };


            function site_informationReq() {
                //this.site_informationReq = function (sId) {
                //siteId = sId;
                responseChannelPrefix = "/" + apiStore + "/" + groupId + "/" + siteId;
                var data = site_information(siteId);
                var publication = client.publish(responseChannelPrefix, data).then(function() {
                    console.log("Successfully published!");

                }, function(error) {
                    console.log("Error publishing: " + error.message);                      ;
                });


            };
            this.customer_informationReq = function() {
                var data = customer_information(sessionStorage.customerID);
                var publication = client.publish(responseChannelPrefix, data).then(function() {
                    console.log("Successfully published!");
                    waitTimeReq = "customer_information";
                    startTimer();
                }, function(error) {
                    console.log("Error publishing: " + error.message);
                    stopLoader();
                });
            }

            // Authenticate Request
            this.authenticateReq = function(username, password) {
                data = authenticate(username, password);

                var publication = client.publish(responseChannelPrefix, data).then(function() {
                    console.log("Successfully published!");
                    waitTimeReq = "authenticate";
                    startTimer();
                }, function(error) {
                    console.log("Error publishing: " + error.message);
                    stopLoader();
                    stopLoader();

                });
            };

            // Authenticate Request
            function authenticateReq(username, password) {
                data = authenticate(username, password);
                var publication = client.publish(responseChannelPrefix, data).then(function() {
                    console.log("Successfully published!");
                    waitTimeReq = "authenticate";
                    startTimer();
                }, function(error) {
                    console.log("Error publishing: " + error.message);
                    stopLoader();

                });
            }
            // UpdateAccount Request
            this.changeAccountReq = function(OldPass, EmailAdd, NewUserName, Newpass) {
                var data = updateAccount(OldPass, EmailAdd, NewUserName, Newpass, sessionStorage.customerID);
                var publication = client.publish(responseChannelPrefix, data).then(function() {
                    console.log("Successfully published!");
                    waitTimeReq = "update_account";
                    startTimer();
                }, function(error) {
                    console.log("Error publishing: " + error.message);
                    stopLoader();
                });
            };


            // Logout Request
            this.logoutReq = function() {
                var data = logout();
                var publication = client.publish(responseChannelPrefix, data).then(function() {
                    console.log("Successfully published!");
                }, function(error) {
                    console.log("Error publishing: " + error.message);
                });
            }

            // Signup Request
            this.signupReq = function(AccountNo, CustomerNo, Username, Password, Email) {
                var data = signup(AccountNo, CustomerNo, Username, Password, Email);
                var publication = client.publish(responseChannelPrefix, data).then(function() {
                    console.log("Successfully published!");
                    waitTimeReq = "signup";
                    startTimer();
                }, function(error) {
                    console.log("Error publishing: " + error.message);
                    stopLoader();

                });
            }

            //Retrieve Login Request
            this.retrieveLoginReq = function(AccountNo, Email) {
                var data = resetPassword(AccountNo, Email);
                var publication = client.publish(responseChannelPrefix, data).then(function() {
                    console.log("Successfully published!");
                    waitTimeReq = "reset_password";
                    startTimer();
                }, function(error) {
                    console.log("Error publishing: " + error.message);
                    stopLoader();

                });
            }


            // paymentHistory Request
            this.payment_historyReq = function() {
                var data = payment_history(sessionStorage.customerID);
                var publication = client.publish(responseChannelPrefix, data).then(function() {
                    console.log("Successfully published!");
                }, function(error) {
                    console.log("Error publishing: " + error.message);

                });
            };

            // signature
            function createSignature(salt, json) {
                // api_key
                var api_key = configDetailsProvider.apiConnect.key;
                var signature = (SHA256(SHA256(SHA256(utf8.encode(json)) + utf8.encode(api_key)) + (salt)));
                return signature;
            };

            // guid
            function createGuid() {

                function _p8(s) {
                    var p = (Math.random().toString(16) + "000000000").substr(2, 8);
                    return s ? "-" + p.substr(0, 4) + "-" + p.substr(4, 4) : p;
                }
                return _p8() + _p8(true) + _p8(true) + _p8();
            };

            // currentdatetime
            function CreateDate() {
                var currentdate = new Date();
                var currentDateTime = currentdate.toISOString();
                //var currentDateTime = (currentdate.toLocaleString()).replace(',', '');
                return currentDateTime;
            };


            //site_information
            function site_information(siteId) {

                var messageid = createGuid();
                var currenttimestamp = CreateDate();
                var data = {
                    "v1": {
                        "returnChannel": responseChannel,
                        "messageID": messageid,
                        "siteID": siteId,
                        "currentTimeStamp": currenttimestamp
                    },
                    "message": "site_information"
                };

                return data;
            };


            //pay_stored_card
            function pay_stored_card(Cardtoken, expDate, first6, last4, cardHolder, cardType, payment, total, processType, customerID) {
                var businessDate = sessionStorage.businessDate;
                var convenienceFree = sessionStorage.convenienceFree;
                var messageid = createGuid();
                var currenttimestamp = CreateDate();
                var data = {
                    v1: {
                        "returnChannel": responseChannel,
                        "messageID": messageid,
                        "siteID": siteId,
                        "customerID": customerID,
                        "token": sessionStorage.validToken,
                        "totalPayment": parseFloat(total),
                        "convenienceFee": parseFloat(convenienceFree),
                        "businessDate": businessDate,
                        "cardInfo": {
                            "cardToken": Cardtoken,
                            "cardType": cardType,
                            "expDate": expDate,
                            "cardHolder": cardHolder,
                            "first6": first6,
                            "last4": last4,
                            "processType": processType
                        },
                        "currentTimeStamp": currenttimestamp,
                        "payments": []
                    },
                    "message": "pay_stored_card"
                };

                for (var i = 0; i < payment.length - 1; i++) {

                    var item = payment[i];
                    data.v1.payments.push({
                        "optionID": item.optionID,
                        "accountType": item.accountType,
                        "accountID": item.accountID,
                        "creditsPaid": item.credits,
                        "amount": item.amountOfPayment,
                        "description": item.paymentDescription
                    });
                }
                return data;
            };

            // Update account
            function updateAccount(OldPass, EmailAdd, NewUserName, Newpass, customerID) {

                var messageid = createGuid();
                var currenttimestamp = CreateDate();
                var data = {
                    "v1": {
                        "returnChannel": responseChannel,
                        "messageID": messageid,
                        "siteID": siteId,
                        "token": sessionStorage.validToken,
                        "customerID": customerID,
                        "oldPassword": OldPass,
                        "newPassword": Newpass,
                        "newUserName": NewUserName,
                        "newEmail": EmailAdd,
                        "currentTimeStamp": currenttimestamp
                    },
                    "message": "update_account"
                };
                return data;
            };

            // logout
            function logout() {

                var messageid = createGuid();
                var currenttimestamp = CreateDate();
                var customerID = sessionStorage.customerID;

                //validToken
                var data = {
                    "v1": {
                        "returnChannel": responseChannel,
                        "messageID": messageid,
                        "siteID": siteId,
                        "customerID": customerID,
                        "token": sessionStorage.validToken,
                        "currentTimeStamp": currenttimestamp
                    },
                    "message": "logout"
                };
                return data;
            };


            //Authentication
            function authenticate(username, password) {
                var messageid = createGuid();
                var currenttimestamp = CreateDate();
                var data = {
                    "v1": {
                        "returnChannel": responseChannel,
                        "messageID": messageid,
                        "siteID": siteId,
                        "userName": username,
                        "password": password,
                        "token": "",
                        "currentTimeStamp": currenttimestamp
                    },
                    "message": "authenticate"
                };
                return data;
            };


            // signup
            function signup(AccountNo, CustomerNo, Username, Password, Email) {
                var messageid = createGuid();
                var currenttimestamp = CreateDate();
                var data = {
                    "v1": {
                        "returnChannel": responseChannel,
                        "messageID": messageid,
                        "siteID": siteId,
                        "accountID": AccountNo,
                        "customerID": CustomerNo,
                        "userName": Username,
                        "password": Password,
                        "email": Email,
                        "currentTimeStamp": currenttimestamp
                    },
                    "message": "signup"
                };
                return data;
            }

            // Reset Password
            function resetPassword(AccountNo, Email) {
                var messageid = createGuid();
                var currenttimestamp = CreateDate();
                var data = {
                    "v1": {
                        "returnChannel": responseChannel,
                        "messageID": messageid,
                        "siteID": siteId,
                        "email": Email,
                        "accountID": AccountNo,
                        "currentTimeStamp": currenttimestamp
                    },
                    "message": "reset_password"
                };

                return data;
            };


            // authenticateRespomse
            function listAccountsReq(msg) {
                var accountInfo = "";
                var data2 = (msg);
                var Message = data2.data.message;
                if (Message == "authenticate") {
                    var error = data2.data.v1.errorDescription.toString();
                    sessionStorage.customerID = data2.data.v1.customerID;
                }
                var accountInfo = listAccounts(sessionStorage.customerID);
//              waitTimeReq = "list_accounts";//F
//              startTimer();//F
                return accountInfo;
            };


            //List Account
            function listAccounts(customerID) {
                var messageid = createGuid();
                var currenttimestamp = CreateDate();
                var data = {
                    "v1": {
                        "returnChannel": responseChannel,
                        "messageID": messageid,
                        "siteID": siteId,
                        "customerID": customerID,
                        "token": sessionStorage.validToken,
                        "currentTimeStamp": currenttimestamp
                    },
                    "message": "list_accounts"
                };

                return data;
            };


            // random128
            function random128() {

                var result = "";
                for (var i = 0; i < 8; i++)
                    result += String.fromCharCode(Math.random() * 0x10000);
                //var salt = Base64.encode(result);
                return result;
            };

            // list_store_cards
            function list_stored_cards(customerID) {

                var messageid = createGuid();
                var currenttimestamp = CreateDate();
                var data = {
                    "v1": {
                        "returnChannel": responseChannel,
                        "messageID": messageid,
                        "siteID": siteId,
                        "customerID": customerID,
                        "token": sessionStorage.validToken,
                        "currentTimeStamp": currenttimestamp
                    },
                    "message": "list_stored_cards"
                };
                return data;
            };


            // customer_information
            function customer_information(customerID) {

                var messageid = createGuid();
                var currenttimestamp = CreateDate();
                var data = {
                    "v1": {
                        "returnChannel": responseChannel,
                        "messageID": messageid,
                        "siteID": siteId,
                        "customerID": customerID,
                        "token": sessionStorage.validToken,
                        "currentTimeStamp": currenttimestamp
                    },
                    "message": "customer_information"
                };
                return data;
            };

            //payment_history
            function payment_history(customerID) {
                var messageid = createGuid();
                var currenttimestamp = CreateDate();
                var data = {
                    "v1": {
                        "returnChannel": responseChannel,
                        "messageID": messageid,
                        "siteID": siteId,
                        "customerID": customerID,
                        "token": sessionStorage.validToken,
                        "currentTimeStamp": currenttimestamp
                    },
                    "message": "payment_history"
                };
                return data;
            }
        } catch (e) {
            // errorMessage redirect to error page.
            console.log(e);
            $location.path("/error").search("ref", e.message);
            $rootScope.Loaded = true;
        }

    }]);
