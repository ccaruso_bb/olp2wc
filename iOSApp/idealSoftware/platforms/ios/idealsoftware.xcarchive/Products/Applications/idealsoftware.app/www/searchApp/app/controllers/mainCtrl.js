angular.module("app")
    .controller("mainCtrl", ["$scope", "$rootScope", "$state", "$window", "SharedService", "$http", "configDetailsProvider","crmOrdersApi", function($scope, $rootScope, $state, $window, SharedService, $http, configDetailsProvider,crmOrdersApi) {

        $rootScope.messageReceived = {
            data: {
                message: ""
            }
        };

        $rootScope.isSubscribe = false;
        $rootScope.ErrorMessage = "";
        $rootScope.EmaiAddress = "";
        $rootScope.phoneNo = "";
        $rootScope.storeINFO = "";
        $rootScope.cartNumber = 0;

        var i = 0;
        try {
            // check api response
            SharedService.removeSession();
            $rootScope.message = "";
            $scope.redirect = function(mode) {
                sessionStorage.setItem("key", mode);
                $window.location.href = "../paymentApp/index.html";

                if ((localStorage.getItem("siteID") != null) && (localStorage.getItem("userName") != null) && (localStorage.getItem("passWord") != null)) {
                    sessionStorage.setItem("siteID", localStorage.getItem("siteID"));
                    sessionStorage.setItem("userName", localStorage.getItem("userName"));
                    sessionStorage.setItem("passWord", localStorage.getItem("passWord"));
                    sessionStorage.loggedUser = "valid";
                }
            }

                if (configDetailsProvider.apiConnect.flag == false) {
                    $(".disabled").css('pointer-events', 'none');
                    $(".disabled").css('opacity', '0.6');
                }

            $(".navbar-brand").css("background", configDetailsProvider.apiConnect.image);
            document.documentElement.style.setProperty('--main-bg-color', configDetailsProvider.apiConnect.color);
            $(".navbar-brand").css("background-size", "contain");

            $scope.$watch(function() {
                return $rootScope.messageReceived;
            }, function(newVal, oldVal) {

                switch (newVal.data.message) {
                    case 'connect':
                        $rootScope.isSubscribe = false;
                        $rootScope.message = "";
                        $rootScope.Loaded = true;
                        //$rootScope.Loaded = false;
                        break;
                    case 'site_information':
                        $rootScope.message = "";
                        if (newVal.data.v1.errorDescription === "Successful") {
                            sessionStorage.checkStoreInformation = "have value";
                            sessionStorage.currentTimeStamp = newVal.data.v1.currentTimeStamp;
                            $rootScope.storeINFO = newVal.data.v1.address + " " + newVal.data.v1.city + " " + " " + newVal.data.v1.state + " " + newVal.data.v1.zip;
                            $rootScope.phoneNo = newVal.data.v1.phone;
                            $rootScope.EmaiAddress = newVal.data.v1.customerServiceEmai;

                            sessionStorage.setItem("locationId", newVal.data.v1.locationID);//F
                            console.log("locationId >>>>>>>>>>>>>>>>>" + JSON.stringify(newVal.data.v1.locationID));//F

                        } else {
                            $rootScope.message = newVal.data.v1.errorDescription;
                            $rootScope.ShowLoader = false;
                        }
                        break;
                    case 'list_categories':
                        $rootScope.isLogin = true;
                        $rootScope.message = "";
                        if (newVal.data.v1.errorDescription != "Successful") {
                            sessionStorage["list_models"] = "";
                            $rootScope.errorCount++;
                            $rootScope.ShowLoader = false;
                            $rootScope.message = newVal.data.v1.errorDescription;
                        } else if (newVal.data.v1.errorDescription === "Successful") {
                            sessionStorage["list_categories"] = JSON.stringify($rootScope.messageReceived);
                            $state.reload("start");
                        } else {
                            $rootScope.message = newVal.data.v1.errorDescription;
                            $rootScope.errorCount = 0;
                            $rootScope.ShowLoader = false;
                        }
                        break;
                    case 'list_models':
                        $rootScope.message = "";
                        $rootScope.ShowLoader = false;
                        if (newVal.data.v1.errorDescription === "Successful") {

                            sessionStorage["list_models"] = JSON.stringify($rootScope.messageReceived);

//                            $scope.list_modelsData = JSON.parse(sessionStorage["list_models"]); // it will require on dataNotFound
//                            var modelsData = $scope.list_modelsData.data.v1.models;
//                            alert(modelsData.length);
                            if ($state.current.name == "start.search" || $state.current.name == "start.search.item") {

                                $state.reload("start.search");

                            } else {

                                $state.go("start.search");
                            }
                        } else {
                            $rootScope.ShowLoader = false;
                            $rootScope.message = newVal.data.v1.errorDescription;
                        }
                        break;

                    case 'list_inventory':
                        if (newVal.data.v1.errorDescription === "Successful") {
                            sessionStorage["list_inventory"] = JSON.stringify($rootScope.messageReceived);
                            if ($state.current.name == "start.search" || $state.current.name == "start.search.item") {
                                $state.reload("start.search");

                            } else {
                                $state.go("start.search");

                            }
                        } else {
                            $rootScope.message = newVal.data.v1.errorDescription;
                        }
                        break;

                    case 'collect_deposit':
                        $rootScope.ShowLoader = false;
                        document.addEventListener("deviceready", onDeviceReady, false);
                        function onDeviceReady() {
                            document.addEventListener("backbutton", function(e) {
                                e.preventDefault();
                            }, false);
                        }

                        if (newVal.data.v1.errorDescription === "Successful") {
                            sessionStorage["collect_deposit"] = JSON.stringify($rootScope.messageReceived);
                           // $state.go("paySuccess");
                           crmOrdersApi.new_opportunityReq();
                        } else {
                            $rootScope.ShowLoader = false;
                            $rootScope.message = newVal.data.v1.errorDescription;
                            $state.go("payDecline");
                        }
                        break;

                       case 'new_opportunity':
                            $rootScope.ShowLoader = false;
                            document.addEventListener("deviceready", onDeviceReady, false);
                            function onDeviceReady() {
                                document.addEventListener("backbutton", function(e) {
                                    e.preventDefault();
                                }, false);
                            }

                            if (newVal.data.v1.errorDescription === "Successful") {
                                sessionStorage["new_opportunity"] = JSON.stringify($rootScope.messageReceived);
                                $state.go("paySuccess");
                            } else {
                                $rootScope.ShowLoader = false;
                                $rootScope.message = newVal.data.v1.errorDescription;
                               // $state.go("payDecline");
                            }
                            break;

                    case 'customer_information':
                        $rootScope.message = "";
                        $rootScope.ShowLoader = false;
                        if (newVal.data.v1.errorDescription === "Successful") {
                            sessionStorage["cust_info"] = JSON.stringify($rootScope.messageReceived);
                            $rootScope.errorCount = 0;
                            $location.path("/paySuccess");
                        } else {
                            $rootScope.ShowLoader = false;
                            $rootScope.message = newVal.data.v1.errorDescription;
                            $state.reload();
                        }

                        break;

                }
            });
        } catch (e) {
            console.log(w);
        }

    }]);
