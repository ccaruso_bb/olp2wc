
    angular.module("app")
        .controller("chatCtrl", ["$scope", "$rootScope", "$window", "$location", "deviceReadyService", "$routeParams", "configDetailsProvider", "SharedService","chatService", function($scope, $rootScope, $window, $location, deviceReadyService, $routeParams, configDetailsProvider, SharedService, chatService) {

            $scope.chat = "";

            var firstParameter;
            var $activeTab;
            var activeId;
            var names = [];
            var tabValue;
            var id;
              
             //for theme color
             document.documentElement.style.setProperty('--main-bg-color', configDetailsProvider.apiConnect.color);
             //for shop hide and show
             $rootScope.IsShopShow = configDetailsProvider.apiConnect.IsShopShow;
                                 
            $(document).ready(function() {                              
                document.documentElement.style.setProperty('--main-bg-color', configDetailsProvider.apiConnect.color);
                              
                var searchObject = $routeParams.siteID;
                if (searchObject != 'undefined') {
                    var url = ($routeParams.siteID).split('=');
                    var firstParameter = '#menu' + url[1];
                    if (firstParameter != '#menuundefined') {
                        activaTab(firstParameter);
                    } else {
                        var searchObject = $routeParams.siteID;
                        var firstParameter = '#menu' + searchObject;
                        activaTab(firstParameter);
                    }

                } else {
                    var firstParameter = '#menu345';
                    activaTab(firstParameter);
                }

                $("ul.tab-nav-sec li a").click(function() {
                    scrollDownFunc();

                });

                tabValue = localStorage.getItem("tab");
                var array = tabValue.split(',');
    //            alert("array ->" + array);
                $.each(array, function( index, value ) {
                  $scope["storearea" + value] = true;
                });
            });

            function activaTab(firstParameter) {
                //$('.tab-nav-sec a[href="#' + firstParameter + '"]').tab('show');
                $('[data-target="' + firstParameter + '"]').tab('show');
            };

            $(".navbar-bg").css("background", configDetailsProvider.apiConnect.color);
            $(".sidebar").css("background", configDetailsProvider.apiConnect.color);
            $("#nav-icon").css("background", configDetailsProvider.apiConnect.color);
            $(".menu").css("background", configDetailsProvider.apiConnect.color);

            if (configDetailsProvider.apiConnect.flag == false) {
                $(".ShopDisabled").css('pointer-events', 'none');
                $(".ShopDisabled").css('opacity', '0.6');
            }

            $scope.about = function() {

                $location.path("/about");
            }
                                 
            var json = JSON.parse(localStorage["store_listFirst"] || "{}");
            $scope.chat = "";
            $scope.data1 = JSON.parse(json.data);
                var siteID = sessionStorage.getItem("siteID");
                var datastoreid = sessionStorage["storeid"];
                var number = "#menu"+siteID;
                $scope["storearea" + number] = true;
    //          if(siteID == null && sessionStorage["notificationMesg"] == undefined)
            if(siteID == null)
              {
                    var num = parseInt(datastoreid.match(/\d+/),10)
                    var menuNum = "#menu" + num;
                    //alert("Num-" + num);
                    $scope["storearea" + num] = true;
                    $('a[data-target= "'+menuNum+'"]').tab('show');
                    sessionStorage.removeItem("storeid");

              }
           else if(siteID != null && sessionStorage["storeid"] == undefined)
              {
                 
                 $scope["storearea" + siteID] = true;
                 $('a[data-target="'+number+'"]').tab('show');
              }

            else if(siteID != null && sessionStorage["storeid"] != undefined)
                {
                var num = parseInt(datastoreid.match(/\d+/),10)
                   var menuNum = "#menu" + num;
                   $scope["storearea" + num] = true;
                   $('a[data-target="'+menuNum+'"]').tab('show');
                   sessionStorage.removeItem("storeid");
                }
                else{}

            // + sign to list store List
            $scope.Storelist = function(){
                $scope.storeList = JSON.parse(json.data);
                $('#selection-popup').modal('show');
                document.documentElement.style.setProperty('--main-bg-color', configDetailsProvider.apiConnect.color);
            }
          //check radio button
            $scope.selectRadio = function(value) {
               SelValue = value;
            };
            //Redirect to added store from store list
             $scope.storeFunc = function() {
                if (SelValue) {
                    console.log("site id=" + SelValue);
                    var i = SelValue;
                    var num = "#menu"+i;
                     if(sessionStorage["siteID"] == i)
                       {
                         $scope["storearea" + i] = true;
                         //sessionStorage["abc"] = $scope["storearea" + i];
                         $('a[data-target="'+num+'"]').tab('show');
                         $("ul.tab-nav-sec li a").ready(function() {
                             scrollDownFunc();
                         });
                       }
                       else
                       {
                          if(tabValue)
                          {
                            names.push(tabValue,i);

                           }
                          else
                          {
                            names.push(i);
                          }
                            var array1 = JSON.parse("[" + names + "]");
                            let uniqueNames = [...new Set(array1)];

                           localStorage["tab"] = uniqueNames;
                           $scope["storearea" + i] = true;
                           $('a[data-target="'+num+'"]').tab('show');

                $("ul.tab-nav-sec li a").ready(function() {
                    scrollDownFunc();
                });

                 }
          }
          }

            $scope.start = function() {
                $(".sidebar").toggleClass("open");
            }

            $scope.redirect = function(mode) {
                sessionStorage.setItem("key", mode);
                $window.location.href = "paymentApp/index.html";
            };
            var itemIndex = 0;
            $rootScope.msge = "";
            var messageCounter = 0;
            var msgh = "";
            $scope.messages = [];
            try {

                $scope.$watch(function() {
                    return $rootScope.messaRe;
                }, function(newVal, oldVal) {


                    if (newVal != undefined) {
                        $scope.messages = [];
                        msgh = JSON.parse(newVal);

                        for (var item = 0; item < msgh.length; item++) {

                            var msgHistory = {
                                data: msgh[item].data,
                                msg: msgh[item].msg,
                                index: msgh[item].index,
                                storeId: msgh[item].store,
                                time: msgh[item].time
                            };

                            $scope.messages.push(msgHistory);

                        };

                        scrollDownFunc();
                    }

                });

                $scope.getStoreId = function(store) {
                    return function(msg) {
                        return msg.storeId == store;
                    }
                }

                function scrollDownFunc() {
                    setTimeout(function() {

                        var $activeTab = $('.tab-content .tab-pane.active');
                        var activeId = $activeTab.attr('id');

                        var myid = $('#' + activeId).find('.chat-view-block').attr('id');

                        var elem = document.getElementById(myid);

                        elem.scrollTop = elem.scrollHeight;

                    }, 1000);
                }

                console.log('fjfgjfgfhj   ' + $scope.messages);

                $scope.send = function(event, number) {

                    var chatMsg = event.chat;
                    event.chat = null;
                                 
                     for(var i = 0; i < $rootScope.myList.length; i++) {
                     
                     if(number==$rootScope.myList[i].locationID)
                     {
                     
                     var obj= $rootScope.myList[i];
                     obj.customer_messageReq(chatMsg);
                     
                     if (chatMsg != undefined) {
                     
                     deviceReadyService.sendMessage(chatMsg, CreateDate(), number);
                     
                     };
                     }
                     
                     }
                            };

                        document.addEventListener("deviceready", onDeviceReady, false);
                         function onDeviceReady() {
                             document.addEventListener("pause", onPause, false)//FF
                             document.addEventListener("resume", onResume, false);//FF
                         }
                                 
                     function onPause() {
                     chatService.clientDisconnect();
                     console.log("----------App is minimised-----------");
                     }
                     
                     function onResume() {
                     console.log("----------App is onResume-----------");
                     
                     }
                                 
                function CreateDate() {
                    var currentdate = new Date();
                    var currentDateTime = currentdate.toISOString();
                    return currentDateTime;
                };
                                 
                                 
                         // chat response watch
                         //check api response...........................
                         $scope.$watch(function() {
                                       return $rootScope.messageReceived;
                                       }, function(newVal, oldVal) {
//                                      alert("msg comming" + newVal.data.message);
                                       //check site_information api method response
                                       if (newVal.data.message === "customer_message") {
                                       $rootScope.message = "";
                                       $rootScope.messageReceived = {
                                       data: {
                                       message: ""
                                       }
                                       };
                                       
                                       $rootScope.customerMsg = "customer_message";
                                       
                                       var resMsg = newVal.data.v1.messageBody;
                                       var siteID = newVal.api_location;
                                       var msgResp = resMsg;
                                       var dateTime = CreateDate();
                                       var storeID = siteID;
//                                       console.log("Watch Data ------" + JSON.stringify(newVal.data));
                                       deviceReadyService.sendMessageResp(msgResp,dateTime,storeID);
                                       
                                       }
//                                       if ($rootScope.testData === true) {
//
//                                       $rootScope.message = "";
//                                       $rootScope.messageReceived = {
//                                       data: {
//                                       message: ""
//                                       }
//                                       };
//
//                                       alert($rootScope.testData);
//
//                                       var data = $rootScope.testData1;
//
//                                       console.log( "----notification. " + JSON.stringify(data) );
//                                       console.log( "----notificationObj1" + data['gcm.notification.siteId']);
//                                       var msgResp = data.aps.alert.body;
//                                       var dateTime = CreateDate();
//                                       var storeID = "1"
//                                       //                            var storeID = data['gcm.notification.siteId'];
//                                       deviceReadyService.sendMessageResp(msgResp,dateTime,storeID);
//
//                                       }
                                       
                                       
                                       });

            } catch (e) {
                console.log(e);
            }

        }]);
