﻿angular.module("app")
    .controller("settingsCtrl", ["$scope", "$rootScope", "$window", "$http", "$location", "SharedService", function($scope, $rootScope, $window, $http, $location, SharedService) {
        try {

            $rootScope.message = "";
            $scope.data = "";
            $scope.testLocationList = function(store_name) {
                console.log(store_name.number);
                SharedService.setData(store_name.number);
                $scope.selectStoreMsg = "Your Store Is Selected";
                sessionStorage.storeName = store_name.name;
            };
            $rootScope.message = "";
            var data = {
                "group": "241",
                "token": "6EE6401237393414807A67AC829B104F16AC45575156CA2077F7EA8A1A59D54A"
            };

            var apiKey = "EB17F8821BEB698B9F46C0D8C22142F8F8146DC72BCA28544722AA9CD6B25FC5AFFEBFA22360E1907877B94D55AB40BE7480EA17E86836B8BFC452F587A6AA89";
            var authentication = SHA256(apiKey + JSON.stringify(data));
            var url = "https://test.idealss.net/lookup_client_locs";
            var config = {
                headers: {
                    "Content-Type": "application/json",
                    "X-Authentication": authentication,
                }
            };

            $http.post(url, data, config)
                .success(function(data, status, headers, config) {
                    $rootScope.message = "Successfull loc list"
                    $scope.data = JSON.parse(data.data);
                    console.log(data);
                })
                .error(function(data, status, headers, config) {
                    $rootScope.message = "Data: " + data +
                        "\n status: " + status.toString() +
                        "\n headers: " + headers.toString() +
                        "\n config: " + config.toString();
                });

        } catch (e) {
            console.log(e);
        }

    }]);