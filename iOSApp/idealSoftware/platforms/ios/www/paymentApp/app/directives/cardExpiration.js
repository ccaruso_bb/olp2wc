﻿
angular.module('app').directive
  ('cardExpiration'
  , function () {
      var directive =
        {
            require: 'ngModel'
        , link: function (scope, elm, attrs, ctrl) {
           
            scope.$watch('[exp_month,exp_year]', function (value) {
                ctrl.$setValidity('invalid', true)

                var aa = parseInt(scope.exp_year);
                var bb = parseInt(scope.currentYear.toString().substr(-2));
                var cc = parseInt(scope.exp_month);
                var dd = scope.currentMonth;


                if (parseInt(scope.exp_year) == parseInt(scope.currentYear.toString().substr(-2))
                     && parseInt(scope.exp_month) < scope.currentMonth
                   ) {
                    ctrl.$setValidity('invalid', false)
                  
                }
                else {
                    ctrl.$setValidity('invalid', true)
                }
                return value
            }, true)
        }
        }
      return directive
  }
    )