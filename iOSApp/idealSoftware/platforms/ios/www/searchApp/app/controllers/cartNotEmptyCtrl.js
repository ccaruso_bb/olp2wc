﻿angular.module("app").
controller("cartNotEmptyCtrl", ["$scope", "$rootScope", "$state", "idealsApi", "$cookieStore",
    function($scope, $rootScope, $state, idealsApi, $cookieStore) {
        try {

            $rootScope.IsVisible = false;
            var cartItem = [];
            $scope.cartItem = [];
            $scope.isDisabled = false;
            var deleteItemId = "";

            $scope.$watch(function() {
                return sessionStorage["cartInfo"];
            }, function(newVal, oldVal) {

                if (newVal == "[]" || newVal == "") {
                    $scope.isDisabled = true;
                }
            });

            if (sessionStorage["cartInfo"] != "") {

                var data = (JSON.parse(sessionStorage["cartInfo"] || "{}"));

                for (var item = 0; item <= data.length - 1; item++) {
                    cartItem.push(data[item]);
                }
                $scope.cartItem = cartItem;
                console.log("$scope.cartItem >>>>>>>>>>>" + JSON.stringify($scope.cartItem));

            };

            //delete item from cart
            $scope.deleteItem = function() {
                var itemId = deleteItemId;
                var index = -1;

                for (var item = 0; item <= cartItem.length - 1; item++) {
                    if (cartItem[item].id == itemId) {
                        index = item;
                        break;
                    }
                }

                cartItem.splice(index, 1);

                $scope.cartInfo = [];

                sessionStorage["cartInfo"] = null;

                var data = cartItem;
                cartItem = [];

                for (var item = 0; item <= data.length - 1; item++) {
                    cartItem.push(data[item]);
                }

                $scope.cartItem = cartItem;
                sessionStorage["cartInfo"] = JSON.stringify(cartItem);
            }

            // remove item from cart
            $scope.itemRemove = function(itemId) {
                deleteItemId = itemId;

                $("#myModal").modal("show");
            }

            // change quantity
            $scope.change_quantity = function(quantity, index) {
                for (var qnty = 0; qnty <= $scope.cartItem.length - 1; qnty++) {
                    if (qnty == index) {
                        $scope.cartItem[qnty].quantity = quantity.quantity;
                    }
                }
                sessionStorage["cartInfo"] = JSON.stringify($scope.cartItem);
            }

            // redirect to items page to select more items
            $scope.continueShopping = function() {
                $state.go("start.search");
            };

            //array of quantutyList
            $scope.quantityList = [{
                    quantity: 1
                },
                {
                    quantity: 2
                },
                {
                    quantity: 3
                },
                {
                    quantity: 4
                },
                {
                    quantity: 5
                }
            ];

        } catch (e) {
            console.log(e);
        }
    }
]);