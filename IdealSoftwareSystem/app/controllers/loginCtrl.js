﻿
angular.module('app')
 .controller('loginCtrl', ['$scope', '$rootScope', '$location', 'idealsApi', '$cookieStore','$http', function ($scope, $rootScope, $location, idealsApi, $cookieStore,$http) {
  
     
     $scope.userName = "";
     $scope.password = "";
 
     try
     {        
         $rootScope.message = "";
         // function to submit the form after all validation has occurred            
         $scope.submitLogin = function () {
            
             // check to make sure the form is completely valid
             if ($scope.loginForm.$valid) {
              
                 $rootScope.ShowLoader = true;
                 $scope.Name = "";
                 var login = {
                     username: $scope.userName,
                     password: $scope.password
                 };

                 if (login.username != "" && login.password != "") {                
                     idealsApi.authenticateReq(login.username, login.password);
                     var data = "";
                 };
             };
         };

         //redirect to RetrieveLoginInfo page.
         $scope.RetrieveLoginInfo = function () {
             $rootScope.message = "";
             $location.path('/RetrieveLoginInfo');
         };

         //redirect to RetrieveLoginInfo page.
         $scope.signUp = function () {
             $rootScope.message = "";
             $location.path('/signUp');
         };


         // check error message
         if ($rootScope.message != "") {     
             $scope.ShowLoader = false;
             $scope.error = $rootScope.message;
         }


         //  Bookmark Page

         $("#Bookmark").click(function (e) {
             var bookmarkURL = window.location.href;
             var bookmarkTitle = document.title;

             if ('addToHomescreen' in window && window.addToHomescreen.isCompatible) {
                 // Mobile browsers
                 addToHomescreen({ autostart: false, startDelay: 0 }).show(true);
             } else if (window.sidebar && window.sidebar.addPanel) {
                 // Firefox version < 23
                 window.sidebar.addPanel(bookmarkTitle, bookmarkURL, '');
             } else if ((window.sidebar && /Firefox/i.test(navigator.userAgent)) || (window.opera && window.print)) {
                 // Firefox version >= 23 and Opera Hotlist
                 $(this).attr({
                     href: bookmarkURL,
                     title: bookmarkTitle,
                     rel: 'sidebar'
                 }).off(e);

                 return true;
             } else if (window.external && ('AddFavorite' in window.external)) {
                 // IE Favorite
                 window.external.AddFavorite(bookmarkURL, bookmarkTitle);
             } else {
                 // Other browsers (mainly WebKit - Chrome/Safari)
                 alert('Press ' + (/Mac/i.test(navigator.userAgent) ? 'Cmd' : 'Ctrl') + '+D to bookmark this page.');
             }
             return false;

         });
     }
     catch(e)
     {
         console.log(e);
     }

}]);