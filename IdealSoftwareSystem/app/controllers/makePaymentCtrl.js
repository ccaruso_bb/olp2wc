﻿angular.module('app')
 .controller('makePayCtrl',['$scope','$rootScope','idealsApi', function ($scope, $rootScope,  idealsApi) {

     try
     {
         $rootScope.message = "";
         // get cust_information api method response and bind data  on html table
         var listCards = JSON.parse(sessionStorage['cust_info'] || '{}');
         var json = listCards.data.v1.customerInfo;
         $scope.customerID = json.customerID;
         $scope.Name = json.firstName + " " + json.lastName;
         $scope.address = json.address1 + " " + json.address2 + "The City," + json.city + " " + json.state + " " + json.zip;

         //var payJson = paymentinfo.data.v1.

         var count = 0;
         $scope.payment = JSON.parse(sessionStorage['paymentAmt'] || '{}');

         for (var i = 0; i < $scope.payment.length; i++) {
             count = count + parseFloat($scope.payment[i].amountOfPayment);
         };

         $scope.total = "$" + (count).toFixed(2);
         var paymentinfo = JSON.parse(sessionStorage['pay_stored_card'] || '{}');

         $scope.approvalCode = paymentinfo.data.v1.approvalCode;
         $scope.paidAmount = $scope.total;
         $scope.receiptNumber = paymentinfo.data.v1.receiptNumber;

         $scope.localDateTime = function (date) {
             var date = new Date(date);
             return date;
         }

         //logout click
         $scope.logoutClick = function () {
             idealsApi.logoutReq();
         };
     }
     catch(e)
     {
         console.log(e);
     }

}]);



