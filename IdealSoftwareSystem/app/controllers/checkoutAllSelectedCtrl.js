﻿angular.module('app')
    .controller('checkoutAllSelectedCtrl', ['$scope', '$rootScope', 'idealsApi', '$location', '$cookieStore', function ($scope, $rootScope, idealsApi, $location, $cookieStore) {
    

     try {
      
         $scope.userFLName = sessionStorage.userFLName;
         var listCards = JSON.parse(sessionStorage['list_cards'] || '{}');
         $rootScope.message = "";
         // check list_cards api method data
  
         if (listCards) {
        
             var count = 0;
             $scope.payment= JSON.parse(sessionStorage['paymentAmt'] || '{}');
             //$scope.payment = sessionStorage.paymentAmt;
             var amountOfPayment = $scope.payment.amountOfPayment;
             var itemDescription = $scope.payment.itemDescription;
             var accountType = $scope.payment.accountType;
             var accountID = $scope.payment.accountID;
             var nextDueDate = $scope.payment.nextDueDate;

             for (var i = 0; i < $scope.payment.length; i++) {
                 count = count + parseFloat($scope.payment[i].amountOfPayment);
             };

             $scope.total = "$" + (count).toFixed(2);

             var json = listCards;
             var cards = json.data.v1.cards;

             // check visibility of card payment and enter card information button
       
             if (cards.length > 0)
             {
               
                 sessionStorage['cards'] = JSON.stringify(cards);
                 $scope.IsVisible1 = true;
                 $scope.IsVisible2 = true;        
             }
             else {
                
                 sessionStorage['cards'] = null;
                 $scope.IsVisible1 = true;
                 $scope.IsVisible2 = false;
             }
         };

         //logout click
         $scope.logoutClick = function () {
             idealsApi.logoutReq();
         };

         $scope.clickNewCardInfo = function () {
             //sessionStorage.paymentAmt = $scope.payment;
             $rootScope.disable = false;
             $location.path('/checkOut');
         };


         $rootScope.PayOptSubmit = function () {
             //var cookieWObject = Apicookies.writePayezzyCookie();
             var cookieWObject = ($cookieStore.get('payeezy_auth'));

             if (cookieWObject)
             {                 
                 $rootScope.disable = false;
                 if (cookieWObject.processor_type == 0) {
                    // alert("Going to pay by payeezy")
                     $rootScope.checkProcessor = 'payeezy';
                     $location.path('/checkOut');
                     
                 }
                 else if (cookieWObject.processor_type == 1) {
                    // alert("Going to pay by cardconnect")
                     $rootScope.checkProcessor = 'cardconnect';                     
                     $location.path('/checkOut');                     
                 }

             }
             else {                 
                 return false;
             }
         };

         //click on saved card button and redirect to checkout page 
         $scope.clickSavedCard = function () {
             $rootScope.disable = true;         
             $location.path('/checkOut');
         };

         $scope.PaymentHistory = function () {
             idealsApi.payment_historyReq();
         };
     }
     catch(e)
     {
         console.log(e);
     }
}]);

