﻿angular.module('app')
 .controller('paymentHistoryCtrl',['$scope', '$rootScope', '$location', 'idealsApi', function ($scope, $rootScope, $location, idealsApi) {

     try {
         $rootScope.message = "";
         $scope.userFLName = sessionStorage.userFLName;
         // get payment_history api method response and bind data  on html table
         var listCards = JSON.parse(sessionStorage['pay_hist'] || '{}');
         var json = listCards.data.v1.payments;
         var paymentHIS = json;
         $scope.payHist = paymentHIS;

         // logout click
         $scope.logoutClick = function () {
             idealsApi.logoutReq();
         };
     }
     catch(e)
     {
         console.log(e);
     }
}]);