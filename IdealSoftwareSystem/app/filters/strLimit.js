﻿angular.module('app')
.filter('strLimit', ['$filter', function ($filter) {
    return function (input, beginlimit, endlimit) {
        if (!input) return;
        if (input.length <= beginlimit + endlimit) {
            return input;
        }

        return $filter('limitTo')(input, -endlimit);
    };
}]);