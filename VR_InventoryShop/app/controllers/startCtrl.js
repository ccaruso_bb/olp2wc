﻿angular.module('app')
 .controller('startCtrl', ['$scope', '$rootScope', '$location', 'idealsApi', '$cookieStore', '$state', function ($scope, $rootScope, $location, idealsApi, $cookieStore, $state) {
     try {
         // check api response 
     
         $scope.productName = "";
         var isSelectedProd = false;
         var isSelectedDept = false;
         $scope.selectedDepartmentName = "";
         $scope.departmentsList = {};
         $scope.product = [];
         var productCode = "";
         $scope.list_categories = JSON.parse(sessionStorage['list_categories'] || '{}');
         var json = $scope.list_categories;
         var department = json.data.v1.departments;
         $scope.cartNumber = 0;
         $scope.selectedDepartment = "";
         $scope.cartInfo = [];
         $scope.departmentsList = department;


         


         // auto search on search button to search product name.
          function autoSearchByDepartment(dept) {
            
              $scope.product = [];

              if (dept==undefined)
              {
                  for (var i = 0; i <= department.length - 1; i++) {

                      for (var j = 0; j <= department[i].productCodes.length - 1; j++) {
                          $scope.product.push(department[i].productCodes[j].description);
                      }
                  }
              }
              else {
                  for (var i = 0; i <= department.length - 1; i++) {

                      if (department[i].department == dept)
                      {
                          for (var j = 0; j <= department[i].productCodes.length - 1; j++) {
                           
                                  $scope.product.push(department[i].productCodes[j].description);
                            
                          }

                          break;
                      }

                  }
              }

          };


         // update cart ......................................
          $scope.cardUpdate = function (data) {

              $scope.cartNumber = $scope.cartNumber + 1;  // update cart 
              $scope.cartInfo.push(data); 
              sessionStorage['cartInfo'] = JSON.stringify($scope.cartInfo);
           
          };


          $scope.cart = function () {
              if ($scope.cartNumber == 0) {
                  $state.go('cartEmpty');
              }
              else {
                  $state.go('cartNotEmpty');
              }

          };


        //check cart info.....................................
          if (sessionStorage['cartInfo'] != "") {

             var cartinfo = JSON.parse(sessionStorage['cartInfo'] || '{}');
            
              $scope.cartNumber = 0;
              for (var i = 0; i <= cartinfo.length - 1; i++) {

                  $scope.cartNumber = $scope.cartNumber + 1;
              }

          };




         // click on serach button to call list_model api method........................................................
         $scope.search = function (selectedDepartment) {
          
             $scope.selectedDepartmentName = selectedDepartment.department;
          
             var product = {
                 product_Name: $scope.prodN.productName
             };
      
             $rootScope.productN = product.product_Name;
             for (var i = 0; i < $scope.product.length; i++)
             {
                 if ($scope.product[i] === product.product_Name) {
                     console.log("I Found something...");
                     isSelectedProd = true;
                     break;

                 }
             }

             if (isSelectedProd == false) {
                 $state.go('start.searchNotFound'); // if product name is wrong 
                 //$state.go('start.search');
             }
             else {

                 for (var i = 0; i < department.length; i++) {

                     if ($scope.selectedDepartmentName === department[i].department) {

                         for (var j = 0; j < department[i].productCodes.length; j++) {

                             if (department[i].productCodes[j].description === product.product_Name) {
                                 idealsApi.list_modelsReq(department[i].productCodes[j].id);
                                 isSelectedProd == false;
                                 break;
                             }
                         }
                     }

                 }

             }
         }


         // this is the solution to keep the drop down list selection after webpage refresh


         for (var i = 0; i < department.length; i++) { //Loop through each array element
          
             for (var j = 0; j <= department[i].productCodes.length - 1; j++) {


                 if (department[i].productCodes[j].description === sessionStorage.getItem("selectedProdN")) { //Check if any of the 'year' value in the array match the value stored in sessionStorage
                   
                     $scope.prodN = {}; //reset the selected scope variable

                     $scope.prodN.productName = department[i].productCodes[j].description; //Assign the array element with the matching year, as the selected scope element
                     break;
                 }
             }
         }
         
         $scope.$watch("prodN.productName", function (prodN) { //Set watch on array element 'year' for changes/selection

             sessionStorage.setItem("selectedProdN", prodN);      //When an element is selected from the drop down list, its value is stored in sessionStorage 

         });

         // this is the solution to keep the drop down list selection after webpage refresh

         for (var i = 0; i < department.length; i++) { //Loop through each array element

             if ($scope.departmentsList[i].department === sessionStorage.getItem("selectedDept")) { //Check if any of the 'year' value in the array match the value stored in sessionStorage

                 $scope.dept = {}; //reset the selected scope variable

                 $scope.dept.selectedDepartment = $scope.departmentsList[i]; //Assign the array element with the matching year, as the selected scope element
             }
         }

         $scope.$watch("dept.selectedDepartment.department", function (dept) { //Set watch on array element 'year' for changes/selection
             autoSearchByDepartment(dept);
             sessionStorage.setItem("selectedDept", dept);      //When an element is selected from the drop down list, its value is stored in sessionStorage 
         });


     }
     catch (e) {
         console.log('error on start page '+e);
     }
 }]);