﻿angular.module('app')
 .controller('searchCtrl', ['$scope', '$rootScope', '$state', 'idealsApi', '$cookieStore', '$compile', function ($scope, $rootScope, $state, idealsApi, $cookieStore, $compile) {
     try {
         // check api response 

         var list_modelsResponse = JSON.parse(sessionStorage['list_models'] || '{}');
         var json1 = list_modelsResponse;
         var modelsList = json1.data.v1.models;

         $scope.Brands = [];
         $scope.WeeklyRates = [];
         $scope.MonthlyRates = [];
         $scope.Price = [];
         $scope.ModelNumber = [];
         $scope.Description = [];



         // bind categories of item..................................................
         for (var i = 0; i < modelsList.length; i++) {



             productBrands = {
                 brand: modelsList[i].brand
             };

             productWeeklyRate = {
                 weeklyRate: modelsList[i].weeklyRate
             };


             productMonthlyRate = {
                 monthlyRate: modelsList[i].monthlyRate
             };

             productPrice = {
                 price: modelsList[i].price
             };

             productModelNumber = {
                 modelNumber: modelsList[i].modelNumber
             };

             productDescription = {
                 description: modelsList[i].description
             };

             $scope.Brands.push(productBrands);
             $scope.WeeklyRates.push(productWeeklyRate);
             $scope.MonthlyRates.push(productMonthlyRate);
             $scope.Price.push(productPrice);
             $scope.ModelNumber.push(productModelNumber);
             $scope.Description.push(productDescription);

         };



         // reset the categories
         $scope.reset = function () {
             $scope.selectedProduct = "";
             $scope.selectedBrand = "";
             $scope.selectedModelNumber = "";
             $scope.selectedPrice = "";
             $scope.selectedWeeklyRate = "";
             $scope.selectedMonthlyRate = "";
         }


         // click on refine search to call inventory details
         $scope.refineSearch = function (selectedModelNumber) {

             for (var i = 0; i < modelsList.length; i++) {

                 if (modelsList[i].modelNumber == selectedModelNumber.modelNumber) {

                     idealsApi.list_inventoryReq(modelsList[i].id);
                     break;
                 }

             }


         }

         // this is the solution to keep the drop down list selection after webpage refresh

         for (var i = 0; i < $scope.ModelNumber.length; i++) { //Loop through each array element

             if ($scope.ModelNumber[i].modelNumber === sessionStorage.getItem("selectedModl")) { //Check if any of the 'year' value in the array match the value stored in sessionStorage

                 $scope.Modl = {}; //reset the selected scope variable

                 $scope.Modl.selectedModelNumber = $scope.ModelNumber[i]; //Assign the array element with the matching year, as the selected scope element
             }
         }

         $scope.$watch("Modl.selectedModelNumber.modelNumber", function (Modl) { //Set watch on array element 'year' for changes/selection
             sessionStorage.setItem("selectedModl", Modl);      //When an element is selected from the drop down list, its value is stored in sessionStorage 
         });

     }

     catch (e) {
         console.log(e);
     }
 }]);