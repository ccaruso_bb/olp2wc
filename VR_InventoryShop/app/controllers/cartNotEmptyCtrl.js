﻿angular.module('app')
 .controller('cartNotEmptyCtrl', ['$scope', '$rootScope', '$state', 'idealsApi', '$cookieStore', function ($scope, $rootScope, $state, idealsApi, $cookieStore) {
     try {
         // check api response 

         if (sessionStorage['cartInfo'] != "") {
         
             $scope.cartinfo = JSON.parse(sessionStorage['cartInfo'] || '{}');
            
         };


         // remove item from cart
         $scope.itemRemove = function (itemId)
         {
            
             var index = -1;
             for (var i = 0; i <= $scope.cartinfo.length - 1;i++)
             {
                 if($scope.cartinfo[i].id==itemId)
                 {
                     index = i;
                     break;
                 }
             }

             $scope.cartinfo.splice(index,1);
         }


         // redirect to items page to select more items
         $scope.continueShopping=function()
         {
             $scope.cartInfo = [];
             sessionStorage['cartInfo'] = 'null';
          


             for (var i = 0; i <= $scope.cartinfo.length - 1; i++) {
                 $scope.cartInfo.push($scope.cartinfo[i]);
                
             }
             sessionStorage['cartInfo'] = JSON.stringify($scope.cartInfo);
             $state.go('start.search.item');
         }
     }
     catch (e) {

     }
 }]);