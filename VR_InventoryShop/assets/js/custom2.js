﻿

// JavaScript Document



$(document).ready(function () {
    resizeContent();
});
$(window).on('resize', function () {
    resizeContent();
});

$(window).on('load', function () {
    resizeContent();
});


function resizeContent() {
    if ($(window).width() >= 768) {
        // if larger or equal
        $('#filtercategory').hide();
        $('.filtercollapse').show();

    } else {
        // if smaller
        $('#filtercategory').show();
        $('.filtercollapse').hide();
    }

}
// category show
$(".linkcollapse").click(function () {
    $(".filtercollapse").slideToggle("slow");
});