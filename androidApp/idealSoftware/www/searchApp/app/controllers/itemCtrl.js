﻿angular.module("app").controller("itemCtrl", ["$scope", "idealsApi", "$cookieStore", "$state", function($scope, idealsApi, $cookieStore, $state) {
    try {

        // respinse of list_inventory method
        var list_inventoryResponse = JSON.parse(sessionStorage["list_inventory"] || "{}");
        var json2 = list_inventoryResponse;
        $scope.inventoryList = json2.data.v1.items;
        var inventoryList = json2.data.v1.items;
        var selectItemDetails = {};
        var itemsArray = [];
        $scope.isDisabled = false;

        // click on new item to select all new items only
        $scope.ClickNewItem = function($event) {
            $scope.inventoryList = [];
            if ($event == true) {
                for (var invty = 0; invty <= (inventoryList).length - 1; invty++) {
                    if (inventoryList[invty].isNew == true) {
                        $scope.inventoryList.push(inventoryList[invty]);
                    }
                }
            } else {
                for (var invty = 0; invty <= (inventoryList).length - 1; invty++) {
                    $scope.inventoryList.push(inventoryList[invty]);
                }
            }
        }

        // range select for item
        $scope.selectRange = function(selectedRange) {
            if (selectedRange == "Cash Price Low to High") {
                $scope.filter = "price";
            } else if (selectedRange == "Cash Price High to Low") {
                $scope.filter = "-price";
            } else if (selectedRange == "Weekly Rate Low to High") {
                $scope.filter = "weeklyRate";
            } else if (selectedRange == "Weekly Rate High to Low") {
                $scope.filter = "-weeklyRate";
            } else if (selectedRange == "Monthly Rate Low to High") {
                $scope.filter = "monthlyRate";
            } else if (selectedRange == "Monthly Rate High to Low") {
                $scope.filter = "-monthlyRate";
            }
        };


        $scope.selectitem = function(data) {
            selectItemDetails = {};
            console.log(data);
            selectItemDetails = data;
            var selModelNumber = sessionStorage.getItem("selectedModl");
            var selProductName = sessionStorage.getItem("selectedProdN");
            var prodID = getProdID(selProductName);
            var modelID = getModelID(selModelNumber);
            selectItemDetails["pcid"] = (prodID);
            selectItemDetails["modid"] = (modelID);
            $scope.cardUpdate(selectItemDetails);
        };


        function getModelID(selModelNumber) {
            var modelID = "";
            var list_modelsResponse = JSON.parse(sessionStorage["list_models"] || "{}");
            var json1 = list_modelsResponse;
            var modelsList = json1.data.v1.models;
            for (var mList = 0; mList <= modelsList.length - 1; mList++) {
                if (modelsList[mList].modelNumber == selModelNumber) {
                    modelID = modelsList[mList].id
                    break;
                }
            };

            return modelID;
        };


        function getProdID(selProductName) {
            var ID = "";
            $scope.list_categories = JSON.parse(sessionStorage["list_categories"] || "{}");
            var json = $scope.list_categories;
            var department = json.data.v1.departments;
            for (var deptL = 0; deptL <= department.length - 1; deptL++) {
                for (var prodC = 0; prodC <= department[deptL].productCodes.length - 1; prodC++) {
                    if (department[deptL].productCodes[prodC].description == selProductName) {
                        ID = department[deptL].productCodes[prodC].id
                        break;
                    }
                }
            }
            return ID;
        };
    } catch (e) {
        console.log(e);
    }
}]);