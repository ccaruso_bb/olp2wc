﻿angular.module("app")
    .controller("errorMsgCtrl", ["$scope", "$window", "$location", "configDetailsProvider","SharedService", function($scope, $window, $location, configDetailsProvider,SharedService) {
        try {

            $scope.start = function() {
                $(".sidebar").toggleClass("open");
            }

            $scope.redirect = function(mode) {
                sessionStorage.setItem("key", mode);
                $window.location.href = "paymentApp/index.html";
            };

            $scope.about = function() {
                $location.path("/about");
            }

          // theme color replaced by config color
           document.documentElement.style.setProperty('--main-bg-color', configDetailsProvider.apiConnect[0].color);

           $rootScope.IsShopShow = configDetailsProvider.apiConnect[0].IsShopShow;

            if (configDetailsProvider.apiConnect[0].flag == false) {
                $(".ShopDisabled").css('pointer-events', 'none');
                $(".ShopDisabled").css('opacity', '0.6');
            }

        } catch (e) {
            console.log(e);
        }
    }]);