﻿angular.module("app")
    .controller("mainCtrl", ["$scope", "$rootScope", "$location", "$route", "idealsApi", "$window", "SharedService", "configDetailsProvider", "$timeout", function($scope, $rootScope, $location, $route, idealsApi, $window, SharedService, configDetailsProvider, $timeout) {

        $rootScope.messageReceived = {
            data: {
                message: ""
            }
        };

        $rootScope.errorCount = 0;
        $rootScope.isLogin = false;
        $rootScope.isSubscribe = false;
        $rootScope.ErrorMessage = "";
        sessionStorage.versionNo = "2.0";
        $rootScope.versionNo = sessionStorage.versionNo;
        $rootScope.mesgGreen=false;
        $rootScope.mesgRed=false;


            var checkConfigData = function() {
            $timeout(function(){
            if(configDetailsProvider != undefined && configDetailsProvider.apiConnect.length>0){
                //Shop Icon hide or Show.
                $rootScope.IsShopShow = configDetailsProvider.apiConnect[0].IsShopShow;

                // theme color replaced by config color
                document.documentElement.style.setProperty('--main-bg-color', configDetailsProvider.apiConnect[0].color);

                if (configDetailsProvider.apiConnect[0].flag == false) {
                    $(".Shopdisabled").css('pointer-events', 'none');
                    $(".Shopdisabled").css('opacity', '0.6');
                }

                $(".main-logo").css("background", configDetailsProvider.apiConnect[0].image);
                $(".main-logo").css("background-size", "contain");
            } else {
            checkConfigData();
            }

            },500)

            }
            checkConfigData();

           if(sessionStorage['privacyPolicy']){
           $rootScope.privacyPolicyEnable = JSON.parse(sessionStorage['privacyPolicy']);
           } else {
           $rootScope.privacyPolicyEnable = true;
           }

         $rootScope.removePrivacy = function() {
           $rootScope.privacyPolicyEnable = false;
           sessionStorage['privacyPolicy'] = $rootScope.privacyPolicyEnable;
           }

           $rootScope.redirectPrivacy = function(){
           $window.location.href = "../index.html#/privacyPolicy";
           }

        try {
            // check api response
            $rootScope.message = "";
            $scope.redirect = function(mode) {
                sessionStorage.setItem("key", mode);
                $window.location.href = "../paymentApp/index.html";
                if ((localStorage.getItem("siteID") != null) && (localStorage.getItem("userName") != null) && (localStorage.getItem("passWord") != null)) {
                    sessionStorage.setItem("siteID", localStorage.getItem("siteID"));
                    sessionStorage.setItem("userName", localStorage.getItem("userName"));
                    sessionStorage.setItem("passWord", localStorage.getItem("passWord"));
                    sessionStorage.loggedUser = "valid";
                }
            }

            $scope.$watch(function() {
                return $rootScope.messageReceived;
            }, function(newVal, oldVal) {


                switch (newVal.data.message) {
                    case 'connect':
                        $rootScope.isSubscribe = false;
                        $rootScope.message = "";

                        if (sessionStorage.getItem("key") != "Payments") {
                            $rootScope.Loaded = true;
                        }
                        break;
                    case 'site_information':
                        $rootScope.message = "";
                        if (newVal.data.v1.errorDescription === "Successful") {
                            $rootScope.apiErrorMessage = "";
                            sessionStorage.checkStoreInformation = "have value";
                            sessionStorage.setItem("siteID", newVal.data.v1.siteID);
                            // sessionStorage.storeName = store_name.name;
                            sessionStorage.currentTimeStamp = newVal.data.v1.currentTimeStamp;
                            sessionStorage.storeINFORMATION = "Store: " + newVal.data.v1.address + " " + newVal.data.v1.city + ", " + " " + newVal.data.v1.state + " " + newVal.data.v1.zip + " " + "Phone: " + newVal.data.v1.phone;
                            $rootScope.storeINFO = sessionStorage.storeINFORMATION;

                        } else {
                            $rootScope.message = newVal.data.v1.errorDescription;
                            $rootScope.ShowLoader = false;
                        }
                        break;

                    case 'authenticate':
                        $rootScope.isLogin = true;
                        $rootScope.message = "";
                        $rootScope.apiErrorMessage = "";
                        if (newVal.data.v1.errorDescription === "Invalid Username and/or Password") {
                            $rootScope.errorCount++;
                            $rootScope.ShowLoader = false;
                            $rootScope.message = newVal.data.v1.errorDescription;
                            $rootScope.mesgRed=true;
                            if ($rootScope.errorCount >= 3) {
                           // $rootScope.msgClass="";
                            // $rootScope.msgClass="error";
                             $rootScope.message = "Invalid username and/or password. If you have forgotten your username and/or password try retrieving it by following the 'Forgot Password' link. If you have not signed up to use this site, you may do so by clicking the Sign Up button.";
                            }
                        } else if (newVal.data.v1.errorDescription === "Successful") {
                            $rootScope.errorCount = 0;
                            sessionStorage.loggedUser = "valid";
                            sessionStorage["authenticate"] = "";
                            sessionStorage["pay_stored_card"] = "";
                            $rootScope.ShowLoader = false;
                            $rootScope.mesgGreen=true;
                            $rootScope.message = "Credentials Verified";
                            var Message = newVal.data.message;
                            if (Message == "authenticate"){
                                sessionStorage.customerID = newVal.data.v1.customerID;
                                sessionStorage.validToken = newVal.data.v1.token;
                            }

                            idealsApi.customer_informationReq();

                            if ($rootScope.rememberMe == true) {
                                var siteID = sessionStorage.getItem("siteID");
                                localStorage.setItem("siteID", siteID);
                                localStorage.setItem("userName", $rootScope.login.username);
                                localStorage.setItem("passWord", $rootScope.login.password);
                            } else {

                                sessionStorage.setItem("userName", $rootScope.login.username);
                                sessionStorage.setItem("passWord", $rootScope.login.password);
                                sessionStorage.loggedUser = "valid";
                                localStorage.removeItem("siteID");
                                localStorage.removeItem("userName");
                                localStorage.removeItem("passWord");
                            }

                        } else {
                            $rootScope.message = newVal.data.v1.errorDescription;
                            $rootScope.errorCount = 0;
                            $rootScope.ShowLoader = false;
                        }
                        break;

                    case 'list_accounts':
                        $rootScope.ShowLoader = false;
                        $rootScope.Loaded = true;

                        if (newVal.data.v1.errorDescription === "Successful") {
                            sessionStorage.removeItem("selectedPayment");
                            sessionStorage.userFLName = newVal.data.v1.customerInfo.firstName + " " + newVal.data.v1.customerInfo.lastName;
                            sessionStorage.userName = newVal.data.v1.customerInfo.userName;
                            sessionStorage["list_accounts"] = JSON.stringify($rootScope.messageReceived);

                            if (sessionStorage.getItem("key") === "Payments") {
                                $rootScope.Loaded = true;
                                $route.reload();
                                sessionStorage.setItem("key", "");
                            } else {
                                $location.path("/account");
                            }
                        } else {
                            $rootScope.ShowLoader = false;
                            $rootScope.message = newVal.data.v1.errorDescription;
                        }
                        break;
                    case 'logout':
                        if (newVal.data.v1.errorDescription === "Successful") {
                            $rootScope.message = "";
                            sessionStorage.loggedUser = "null";
                            $location.path("/login");
                        } else {
                            $rootScope.message = "";
                            $rootScope.ShowLoader = false;
                            $route.reload();
                            $rootScope.message = newVal.data.v1.errorDescription;
                        }
                        break;
                    case 'update_account':
                        $rootScope.message = "";
                        newVal.data.message = "";
                        $rootScope.ShowLoader = false;
                        if (newVal.data.v1.errorDescription === "Successful") {
                            $rootScope.accountSubmitMessage = "Your Account has been updated.";
                            $location.path("/changeAcSubmit");
                        } else {
                            $rootScope.message = "";
                            $rootScope.ShowLoader = false;
                            $route.reload();
                            $rootScope.message = newVal.data.v1.errorDescription;
                        }
                        break;
                    case 'reset_password':
                        $rootScope.message = "";
                        $rootScope.ShowLoader = false;
                        if (newVal.data.v1.errorDescription === "Successful") {
                            $rootScope.accountSubmitMessage = "Your Account has been updated.";
                            $location.path("/changeAcSubmit");
                        } else {
                            $rootScope.message = newVal.data.v1.errorDescription;
                            $rootScope.ShowLoader = false;
                            $route.reload();
                        }
                        break;
                    case 'signup':
                        $rootScope.message = "";
                        $rootScope.ShowLoader = false;
                        if (newVal.data.v1.errorDescription === "Successful") {
                            $rootScope.errorCount = 0;
                            $rootScope.accountSubmitMessage = "Your Account has been created. Please make a note of your username and password.";
                            $location.path("/changeAcSubmit");
                        } else {
                            $rootScope.ShowLoader = false;
                            $rootScope.message = newVal.data.v1.errorDescription;
                            $route.reload();
                        }
                        break;
                    case 'list_stored_cards':
                        $rootScope.message = "";
                        $rootScope.ShowLoader = false;
                        if (newVal.data.v1.errorDescription === "Successful") {
                            if (newVal.data.v1.cards != null) {
                                sessionStorage["list_cards"] = JSON.stringify($rootScope.messageReceived);
                            } else {
                                sessionStorage["list_cards"] = null;
                            }
                            $rootScope.errorCount = 0;
                            $location.path("/checkInfo");
                        } else {
                            $rootScope.ShowLoader = false;
                            $rootScope.message = newVal.data.v1.errorDescription;
                            $route.reload();
                        }
                        break;
                    case 'pay_stored_card':
                        $rootScope.message = "";
                        sessionStorage.removeItem("selectedPayment");
                        $rootScope.ShowLoader = false;


                        //...........android and ios back button diable ..................
                        document.addEventListener("deviceready", onDeviceReady, false);

                        function onDeviceReady() {
                            document.addEventListener("backbutton", function(e) {
                                e.preventDefault();
                            }, false);
                        }
                        //...............ended................

                        if (newVal.data.v1.errorDescription === "Successful") {
                            sessionStorage["pay_stored_card"] = JSON.stringify($rootScope.messageReceived);
                            $rootScope.errorCount = 0;
                        } else {
                            $rootScope.ShowLoader = false;
                            $rootScope.message = newVal.data.v1.errorDescription;
                            $location.path("/paymentDecline");
                        }
                        break;
                    case 'customer_information':
                        if (sessionStorage["pay_stored_card"] === "") {
                            sessionStorage.customerInformation = JSON.stringify(newVal.data.v1.customerInfo);

                            $rootScope.firstName = JSON.parse(sessionStorage.customerInformation).firstName;
                            $rootScope.lastName = JSON.parse(sessionStorage.customerInformation).lastName;
                        } else {
                            $rootScope.message = "";
                            $rootScope.ShowLoader = false;

                            //.......android and ios back button diable ..............

                            document.addEventListener("deviceready", onDeviceReady, false);

                            function onDeviceReady() {
                                document.addEventListener("backbutton", function(e) {
                                    e.preventDefault();
                                }, false);
                            }
                            //....................ended......................

                            if (newVal.data.v1.errorDescription === "Successful") {
                                sessionStorage["cust_info"] = JSON.stringify($rootScope.messageReceived);
                                $rootScope.errorCount = 0;
                                $location.path("/makePayment");
                            } else {
                                $rootScope.ShowLoader = false;
                                $rootScope.message = newVal.data.v1.errorDescription;
                                $route.reload();
                            }
                        }
                        break;

                    case 'payment_history':

                        $rootScope.message = "";
                        if (newVal.data.v1.errorDescription === "Successful") {
                            sessionStorage["pay_hist"] = JSON.stringify($rootScope.messageReceived);
                            $rootScope.errorCount = 0;
                            $location.path("/PaymentHistory");
                        } else {
                            $rootScope.ShowLoader = false;
                            $rootScope.message = newVal.data.v1.errorDescription;
                            $route.reload();
                        }
                        break;
                }

            });
        } catch (e) {
            console.log(e);
        }

    }]);