﻿angular.module("app")
    .controller("accountCtrl", ["$rootScope", "$scope", "$location", "idealsApi", function($rootScope, $scope, $location, idealsApi) {

        var SelValue = "";
        var countpay = 0;
        var person;
        $scope.isVisibleRemoveAll = true;
        try {
             $('#hamburger-inner-navbar').click(function () {
                $("#inner-navbar").toggleClass("in");
            });

            $rootScope.isEnableStoreSel = sessionStorage.isEnableStoreSel;
            $rootScope.setValue = sessionStorage.setValue;
            $rootScope.message = ""
            $scope.userFLName = sessionStorage.userFLName;
            $scope.$watch(function() {
                return ($scope.payment, $scope.footerCount);
            }, function(newVal, oldVal) {

                if (newVal) {
                    $scope.IsVisible1 = true;
                    $scope.IsVisible2 = false;
                } else {
                    $scope.IsVisible1 = false;
                    $scope.IsVisible2 = true;
                }
            });

            // check message of list_account
            $scope.messageReceived = JSON.parse(sessionStorage["list_accounts"] || "{}");
            if ($scope.messageReceived) {

                var counts = 0;
                $scope.count = 0;
                var json = $scope.messageReceived;
                $scope.businessDate = localDateTime(json.data.v1.businessDate);
                var accountsData = json.data.v1.accounts;
                $scope.account = [];
                var dueDate = "";
                var accountType = "";
                var accountID = "";
                var itemDescription = "";
                var modeOfPayment = "";
                var balanceDue = "";
                var numberOfPayments = "";
                var paymentOptions = "";
                var paymentAllowed;

                if (json.data.v1.forceMinDue == true && sessionStorage.getItem("selectedPayment") == null) {

                    $scope.isVisibleRemoveAll = false;
                    callPayMinDue(accountsData);
                } else if (sessionStorage["selectedPayment"]) {
                    $scope.payment = JSON.parse(sessionStorage["selectedPayment"] || "{}");
                    var count = 0;

                    for (var item = 0; item < $scope.payment.length; item++) {
                        count = count + parseFloat($scope.payment[item].amountOfPayment);
                    };
                    var countpay = $scope.payment.length;
                    $scope.footerCount = "$" + (count).toFixed(2);
                } else {
                    countpay = 0;
                    $scope.payment = "";
                    $scope.footerCount = "";
                }

                //count account array length
                for (var item = 0; item < accountsData.length; item++) {
                    //create account information json to bind table
                    var accountInfo = {
                        paymentAllowed: accountsData[item].paymentAllowed,
                        amountOfPayment: accountsData[item].subTotal,
                        numberOfPayments: accountsData[item].numberOfPayments,
                        dueDate: localDateTime(accountsData[item].dueDate),
                        accountType: accountsData[item].accountType,
                        accountID: accountsData[item].accountID,
                        itemDescription: accountsData[item].itemDescription,
                        modeOfPayment: accountsData[item].modeOfPayment,
                        paymentOptions: accountsData[item].paymentOptions
                    };

                    $scope.account.push(accountInfo);
                };

                //count total amount to show in html footer.
                for (var item = 0; item < $scope.account.length; item++) {
                    counts = (counts) + parseFloat($scope.account[item].amountOfPayment);
                };

                $scope.count = "$" + (counts).toFixed(2);

                // click pay button to open payment popup window
                $scope.payButton = function(accounts) {
                    $("input:radio").attr("checked", false);
                    SelValue = "";
                    $scope.paymentOption = accounts.paymentOptions;
                    $scope.messageInfo = (accounts.accountType) + " " + accounts.accountID;
                    $('#myModal').modal('show');
                };

                //click detail button to open detail popup window
                $scope.detailButton = function(accounts) {
                    var accountsArr = json.data.v1.accounts;

                    for (var item = 0; item < accountsArr.length; item++) {
                        if (accounts.accountID == accountsArr[item].accountID) {
                            $scope.messageInfo = (accountsArr[item].accountType) + " " + accountsArr[item].accountID;
                            $scope.detailInventory = accountsArr[item].inventory;
                            $scope.paymentFreq = accountsArr[item].modeOfPayment;
                            $scope.RemBal = "$" + accountsArr[item].balanceDue;
                            $scope.paymentallowed = accountsArr[item].paymentAllowed;
                            if (parseFloat(typeof(accountsArr[item].term) == "undefined" ? 0 : (accountsArr[item].term)) == 0.0) {
                                $scope.RemBal = 0;
                                $scope.terms = (parseFloat(typeof(accountsArr[item].rentalRate) == "undefined" ? 0 : (accountsArr[item].rentalRate)) + parseFloat(typeof(accountsArr[item].fees) == "undefined" ? 0 : (accountsArr[item].fees)) + parseFloat(typeof(accountsArr[item].taxes) == "undefined" ? 0 : (accountsArr[item].taxes))).toFixed(2) + " = Rate: $" + parseFloat(typeof(accountsArr[item].rentalRate) == "undefined" ? 0 : (accountsArr[item].rentalRate)) + " + fees: $" + parseFloat(typeof(accountsArr[item].fees) == "undefined" ? 0 : (accountsArr[item].fees)) + " + Tax: $" + parseFloat(typeof(accountsArr[item].taxes) == "undefined" ? 0 : (accountsArr[item].taxes));
                            } else {
                                $scope.terms = parseFloat(typeof(accountsArr[item].term) == "undefined" ? 0 : (accountsArr[item].term)) + " Payments at  $" + (parseFloat(typeof(accountsArr[item].rentalRate) == "undefined" ? 0 : (accountsArr[item].rentalRate)) + parseFloat(typeof(accountsArr[item].fees) == "undefined" ? 0 : (accountsArr[item].fees)) + parseFloat(typeof(accountsArr[item].taxes) == "undefined" ? 0 : (accountsArr[item].taxes))).toFixed(2) + " = Rate: $" + parseFloat(typeof(accountsArr[item].rentalRate) == "undefined" ? 0 : (accountsArr[item].rentalRate)) + " + fees: $" + parseFloat(typeof(accountsArr[item].fees) == "undefined" ? 0 : (accountsArr[item].fees)) + " + Tax: $" + parseFloat(typeof(accountsArr[item].taxes) == "undefined" ? 0 : (accountsArr[item].taxes));
                            }

                            break;
                        };
                    };

                    $("#paymentDetail").modal("show");
                };

                //check radio button
                $scope.selectRadio = function(value) {
                    SelValue = value;
                };

                var amountOfPayment1 = "";
                var nextDueDate1 = "";
                var accountID1 = "";
                var accountType1 = "";
                var itemDescription1 = "";
                var credits1;
                var optionID1;
                var paymentDescription1;


                //select payment detail from pay popup window and selected information bind in "selected_payment" table
                $scope.pay = function() {
                    $scope.disableButton = false;

                    if (SelValue) {

                        for (var item = 0; item < $scope.paymentOption.length; item++) {
                            var obj = $scope.paymentOption[item];
                            if (obj.credits === SelValue) {
                                amountOfPayment1 = obj.amountOfPayment;
                                nextDueDate1 = localDateTime(obj.nextDueDate);
                                credits1 = obj.credits,
                                    optionID1 = obj.optionID,
                                    paymentDescription1 = obj.paymentDescription
                                break;
                            }
                        }

                        for (var item = 0; item < accountsData.length; item++) {
                            var payOption = accountsData[item].paymentOptions;
                            if (payOption === $scope.paymentOption) {
                                accountID1 = accountsData[item].accountID;
                                accountType1 = accountsData[item].accountType;
                                itemDescription1 = accountsData[item].itemDescription;
                                break;
                            }
                        }
                        if (countpay == 0) {
                            person = [{
                                itemDescription: itemDescription1,
                                amountOfPayment: amountOfPayment1,
                                nextDueDate: localDateTime(nextDueDate1),
                                accountID: accountID1,
                                accountType: accountType1,
                                credits: credits1,
                                optionID: optionID1,
                                paymentDescription: paymentDescription1
                            }];
                            $scope.payment = person;
                            countpay++;
                            footerCount();
                        } else {
                            var index = 0;
                            for (index = 0; index < $scope.payment.length; index++) {
                                if ($scope.payment[index].accountID === accountID1) {
                                    var amount = ($scope.payment[index].amountOfPayment);
                                    if ($scope.payment[index].amountOfPayment == amountOfPayment1) {
                                        break;
                                    } else {
                                        $scope.removeUpdRow($scope.payment[index].accountID);

                                        if ($scope.payment) {
                                            person = {
                                                itemDescription: itemDescription1,
                                                amountOfPayment: amountOfPayment1,
                                                nextDueDate: localDateTime(nextDueDate1),
                                                accountID: accountID1,
                                                accountType: accountType1,
                                                credits: credits1,
                                                optionID: optionID1,
                                                paymentDescription: paymentDescription1
                                            };
                                            $scope.payment.push(person);

                                        } else {
                                            person = [{
                                                itemDescription: itemDescription1,
                                                amountOfPayment: amountOfPayment1,
                                                nextDueDate: localDateTime(nextDueDate1),
                                                accountID: accountID1,
                                                accountType: accountType1,
                                                credits: credits1,
                                                optionID: optionID1,
                                                paymentDescription: paymentDescription1
                                            }];
                                            $scope.payment = person;
                                            countpay++;
                                        }
                                        footerCount();
                                        break;
                                    }
                                }
                            }

                            if (index == $scope.payment.length) {
                                person = {
                                    itemDescription: itemDescription1,
                                    amountOfPayment: amountOfPayment1,
                                    nextDueDate: localDateTime(nextDueDate1),
                                    accountID: accountID1,
                                    accountType: accountType1,
                                    credits: credits1,
                                    optionID: optionID1,
                                    paymentDescription: paymentDescription1
                                };
                                $scope.payment.push(person);
                                footerCount();
                            }
                        };
                    }
                };

                //remove selected row
                $scope.removeUpdRow = function(accountID) {
                    var index = -1;
                    var paymentArr = eval($scope.payment);
                    for (var item = 0; item < paymentArr.length; item++) {
                        if (paymentArr[item].accountID === accountID) {
                            index = item;
                            break;
                        }
                    }
                    if (index === -1) {
                        alert("Something gone wrong");
                    }

                    $scope.payment.splice(index, 1);

                };

                //remove row
                $scope.removeRow = function(accountID) {
                    var index = -1;
                    var paymentArr = eval($scope.payment);
                    for (var item = 0; item < paymentArr.length; item++) {
                        if (paymentArr[item].accountID === accountID) {
                            index = item;
                            break;
                        }
                    }
                    if (index === -1) {
                        alert("Something gone wrong");
                    }
                    $scope.payment.splice(index, 1);

                    footerCount();
                };

                //remove all select payment
                $scope.removeALLRow = function() {
                    countpay = 0;
                    $scope.payment = "";
                    $scope.footerCount = "";
                    //footerCount();
                };

                // payMinDue functionlity based on condition
                $scope.payMinDue = function() {
                    callPayMinDue(accountsData);
                };

                //checkout click to send payment detail and call list_store_card api method.
                $scope.checkOut = function() {
                    sessionStorage["selectedPayment"] = JSON.stringify($scope.payment);
                    $scope.Selamount = [];
                    $scope.Selamount = $scope.payment.slice(0);
                    var convenfree = {
                        itemDescription: "Convenience Fee",
                        accountID: "",
                        accountType: "",
                        amountOfPayment: $scope.messageReceived.data.v1.convenienceFee,
                        nextDueDate: "",
                        credits: "",
                        optionID: "",
                        paymentDescription: ""
                    };

                    $scope.Selamount.push(convenfree);
                    sessionStorage["paymentAmt"] = JSON.stringify($scope.Selamount);
                    sessionStorage.businessDate = $scope.businessDate;
                    sessionStorage.convenienceFree = $scope.messageReceived.data.v1.convenienceFee;
                    idealsApi.list_stored_cardsReq();
                };

                // call payment_history api method
                $scope.paymentHistory = function() {
                    idealsApi.payment_historyReq();
                }

                function callPayMinDue(accountsData) {
                    var isFirst = true;
                    for (var item = 0; item < accountsData.length; item++) {
                        if (accountsData[item].subTotal > 0 && accountsData[item].paymentAllowed == true) {
                            if (isFirst) {
                                $scope.payment = [];
                            }
                            $scope.disableButton = true;
                            person = {
                                itemDescription: accountsData[item].itemDescription,
                                accountID: accountsData[item].accountID,
                                accountType: accountsData[item].accountType,
                                amountOfPayment: accountsData[item].paymentOptions[0].amountOfPayment,
                                nextDueDate: localDateTime(accountsData[item].paymentOptions[0].nextDueDate),
                                credits: accountsData[item].paymentOptions[0].credits,
                                optionID: accountsData[item].paymentOptions[0].optionID,
                                paymentDescription: accountsData[item].paymentOptions[0].paymentDescription
                            };
                            $scope.payment.push(person);
                            isFirst = false;
                        }
                    }
                    footerCount();
                }

                 function localDateTime(date) {
                                    console.log("before ist= " + date);
                                    var date = new Date(date);
                                    // toLocaleDateString() without arguments depends on the implementation,
                                    // the default locale, and the default time zone
                                    console.log("after ist=" + date);
                                    return date;
                                    // → "12/11/2012" if run in en-US locale with time zone America/Los_Angeles
                                }

                                   //count footer of selected payment amount
                function footerCount() {
                        var fcount = 0;
                        var paymentArr = eval($scope.payment);
                        if (paymentArr) {
                            for (var item = 0; item < paymentArr.length; item++) {
                                fcount = fcount + (parseFloat(paymentArr[item].amountOfPayment));
                            }
                        } else {
                            $scope.payment = "";
                        }

                        if (fcount == 0) {
                            $scope.footerCount = "";
                        } else {
                            $scope.footerCount = "$" + fcount.toFixed(2);
                        }
                };

            }


        } catch (e) {
            console.log(e);
        }

    }]);