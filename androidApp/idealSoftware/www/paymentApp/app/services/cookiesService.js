﻿angular.module("app")
    .service("Apicookies", ["$rootScope", "$cookieStore", "$location", function($rootScope, $cookieStore, $location) {
        try {

            this.writeApiCookie = function() {
                var report_url = "";
                if (sessionStorage.report_url) {
                    report_url = JSON.parse(sessionStorage["report_url"] || "{}");
                } else {
                    report_url = "";
                };
                var api_connect = {
                    "report_url": "https://test.idealss.net/report_a_problem",
                    "location_id": 1,
                    "env": "development"
                };
                $cookieStore.put("api_connect", api_connect);
                return api_connect;

            };

         this.writePayezzyCookie = function () {

            var payeezy_auth = {
                processor_type: 1,
                payeezy_key: "js-6125e57ce5c46e10087a545b9e9d7354c23e1a1670d9e9c7",
                api_key: "y6pWAJNyJyjGv66IsVuWnklkKUPFbb0a",
                ta_token: "NOIW",
                cc_api_key:"y6pWAJNyJyjGv66IsVuWnklkKUPFbb0a",// [string],
                cc_merch_id: "800000000175"//"496160873888"//[string]
             }
             $cookieStore.put("payeezy_auth", payeezy_auth);
             return payeezy_auth;
         }

            //function to remove cookie
            this.removeCookie = function() {
                $cookieStore.remove("api_connect");
                $cookieStore.remove("payeezy_auth");
            };
        } catch (e) {
            console.log(e);
        }
    }]);