package com.red_folder.phonegap.plugin.backgroundservice;
import android.app.NotificationManager;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;
import net.idealss.api.BayeuxSubscriptionHandler;
import net.idealss.api.IdealAPI;
import org.json.JSONException;
import org.json.JSONObject;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.UUID;


public class MyService extends BackgroundService implements MesaageListener {

    IdealAPI idealAPI;
    public List<MultiStoreConn> Stores;
    DatabaseHandler db = new DatabaseHandler(this);
    private String KEY_REPLY = "reply_key";
    private String replyMsg = "";
    public JSONObject JsonResult;
    private String deviceID = "863194038647266";
    private String msgReq = "";
    private String msgResp = "";
    private String dateTime = "";
    private String token = "";
    private int storeID;
    private String clientID;

    @Override
    protected JSONObject initialiseLatestResult() {
        //onTimerDisabled();

        JSONObject result = new JSONObject();
        return result;
    }

    @Override
    protected JSONObject doWork() {
        JSONObject result = new JSONObject();

        try {
            // Following three lines simply produce a text string with Hello World and the date & time (UK format)
            // SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            //String now = df.format(new Date(System.currentTimeMillis()));
            String msg = "";
            onTimerEnabled();
            if (JsonResult != null) {
                result.put("JsonResponse", JsonResult);
                //  Log.d("doWork...", JsonResult);
            }
            // We output the message to the logcat
            Log.d("MyService", msg);

            // We also provide the same message in our JSON Result
            // result.put("JsonResponse", msg);
        } catch (JSONException e) {
            // In production code, you would have some exception handling here
            e.printStackTrace();
        }

        return result;
    }

    @Override
    protected JSONObject getConfig() {

        return null;
    }

    @Override
    protected void setConfig(JSONObject config) {
        try {
            if (config.has("key")) {
                String value = config.getString("key");
                int locationID = Integer.parseInt(config.getString("locationID"));
                Log.d("setGroup...", value);

                if (value != "undefined" && value != null) {
                    Log.e("configGGGG....", value);
                    Log.e("config....", value);
                    msgReq = value;
                    int id = db.getStoreIndex(locationID);
                    MultiStoreConn obj = Stores.get(id - 1);
                    obj.customer_messageReq(value);
                    //JsonResult="message";
                }
                config.put("key", null);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void runOnce() {
        super.runOnce();
    }


    public String toISO8601Date(Date date) {
        TimeZone tz = TimeZone.getTimeZone("UTC");
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd\'T\'HH:mm:ss");
        df.setTimeZone(tz);
        return df.format(date);
    }

    private String getGuid() {
        UUID uuid = UUID.randomUUID();
        String randomUUIDString = uuid.toString();
        String messageid = randomUUIDString;
        return messageid;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {

        try {
            if (Stores == null) {


                Stores = new ArrayList<MultiStoreConn>();

                Cursor cursor = db.getStore();

                if (cursor.moveToFirst()) {
                    while (!cursor.isAfterLast()) {
                        int storeId = Integer.parseInt(cursor.getString(cursor.getColumnIndex("count")));
                        String api = (cursor.getString(cursor.getColumnIndex("apiName")));
                        int group = Integer.parseInt(cursor.getString(cursor.getColumnIndex("groupId")));
                        String privateKey = (cursor.getString(cursor.getColumnIndex("privateKey")));
                        String token = (cursor.getString(cursor.getColumnIndex("apiToken")));
                        String client_Url = (cursor.getString(cursor.getColumnIndex("clientUrl")));
                        String email = (cursor.getString(cursor.getColumnIndex("email")));
                        String phoneNumber = (cursor.getString(cursor.getColumnIndex("phoneNumber")));
                        String deviceID = (cursor.getString(cursor.getColumnIndex("deviceID")));
                        String devicePlatform = (cursor.getString(cursor.getColumnIndex("devicePlatform")));

                        // do what ever you want here
                        Stores.add(new MultiStoreConn(this, storeId,api,group,privateKey,token,client_Url,email,phoneNumber,deviceID,devicePlatform ,this));
                        cursor.moveToNext();
                    }
                }
                cursor.close();

            } else {

            }


            if (intent != null) {
                Bundle remoteInput = android.support.v4.app.RemoteInput.getResultsFromIntent(intent);

                if (remoteInput != null) {

                    CharSequence charSequence = remoteInput.getCharSequence(
                            KEY_REPLY);

                    if (charSequence != null) {
                        int StoreID = 0;
                        //Set the inline reply text in the TextView
                        Bundle extras = intent.getExtras();
                        if (intent.getExtras() != null) {

                            StoreID = intent.getExtras().getInt("myMSg");
                            int id = db.getStoreIndex(StoreID);

                            MultiStoreConn obj = Stores.get(id - 1);


                            replyMsg = charSequence.toString();
                            Toast.makeText(this, replyMsg, Toast.LENGTH_LONG).show();

                            //   notificationManager.cancel(NOTIFICATION_ID);

                            // msgResp = JsonResult;
                            msgResp = "";
                            dateTime = toISO8601Date(new Date());
                            msgReq = replyMsg;
                            msgResp = "";
                            token = "";
                            storeID = obj.idealAPI.getLocationId();
                            clientID = obj.idealAPI.getClientID();

                            db.addContact(new Notifications(deviceID, msgReq, msgResp, dateTime, token, storeID, clientID));
                            //customer_messageReq(replyMsg);
                            obj.customer_messageReq(replyMsg);
                        }

                    }
                }

            }


            final Handler handler = new Handler();
            final int delay = 9000; //milliseconds

            handler.postDelayed(new Runnable() {
                public void run() {
                    //do something
                    // Toast.makeText(MyService.this, "service running", Toast.LENGTH_SHORT).show();
                    handler.postDelayed(this, delay);
                }
            }, delay);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

        return START_STICKY;
    }


    @Override
    public void onMesasgeRecived(JSONObject jsonObject) {
        JsonResult = jsonObject;
        runOnce();
    }


}