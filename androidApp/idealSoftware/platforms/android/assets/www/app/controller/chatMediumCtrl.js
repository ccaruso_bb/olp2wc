﻿
    angular.module("app")
        .controller("chatCtrl", ["$scope", "$rootScope", "$window", "$location", "deviceReadyService", "$routeParams", "configDetailsProvider", "$http", "SharedService", "$timeout", function($scope, $rootScope, $window, $location, deviceReadyService, $routeParams, configDetailsProvider, $http, SharedService, $timeout) {

            $scope.chat = "";
            var firstParameter;
            var $activeTab;
            var activeId;
            var names = [];
            var tabValue;

            var checkConfigData = function() {
            $timeout(function(){
            if(configDetailsProvider != undefined && configDetailsProvider.apiConnect.length>0){
                // theme color replaced by config color
                document.documentElement.style.setProperty('--main-bg-color', configDetailsProvider.apiConnect[0].color);
                $rootScope.IsShopShow = configDetailsProvider.apiConnect[0].IsShopShow;
                if (configDetailsProvider.apiConnect[0].flag == false) {
                    $(".ShopDisabled").css('pointer-events', 'none');
                    $(".ShopDisabled").css('opacity', '0.6');
                }
            } else {
            checkConfigData();
            }

            },500)

            }
            checkConfigData();

            var id;
            $(document).ready(function() {

                var searchObject = $routeParams.siteID;
                if (searchObject != 'undefined') {
                    var url = ($routeParams.siteID).split('=');
                    var firstParameter = '#menu' + url[1];
                    if (firstParameter != '#menuundefined') {
                        activaTab(firstParameter);
                    } else {
                        var searchObject = $routeParams.siteID;
                        var firstParameter = '#menu' + searchObject;
                        activaTab(firstParameter);
                    }

                } else {
                    var firstParameter = '#menu345';
                    activaTab(firstParameter);
                }

                $("ul.tab-nav-sec li a").click(function() {
                    scrollDownFunc();
                });

                tabValue = localStorage.getItem("tab");
                var array = tabValue.split(',');
                $.each(array, function( index, value ) {
                  $scope["storearea" + value] = true;
                });
            });

            function activaTab(firstParameter) {
                $('[data-target="' + firstParameter + '"]').tab('show');
            };


            $scope.about = function() {
                $location.path("/about");
            }

            var json = JSON.parse(localStorage["store_listFirst"] || "{}");
            $scope.chat = "";
            $scope.data1 = JSON.parse(json.data);

           // Store to Appears in chart Tab
            var siteID = sessionStorage.getItem("siteID");
            var dataStoreId = sessionStorage["storeid"];
            var number = "#menu"+siteID;
            $scope["storearea" + number] = true;

//          if(siteID == null && sessionStorage["notificationMesg"] == undefined)
            if(siteID == null)
              {
                    var num = parseInt(dataStoreId.match(/\d+/),10)
                    var menuNum = "#menu" + num;
                    $scope["storearea" + num] = true;
                    $('a[data-target= "'+menuNum+'"]').tab('show');
                    sessionStorage.removeItem("storeid");
              }
             else if(siteID != null && sessionStorage["storeid"] == undefined)
              {
                 $scope["storearea" + siteID] = true;
                 $('a[data-target="'+number+'"]').tab('show');

              }

             else if(siteID != null && sessionStorage["storeid"] != undefined)
                {
                var num = parseInt(dataStoreId.match(/\d+/),10)
                   var menuNum = "#menu" + num;
                   $scope["storearea" + num] = true;
                   $('a[data-target="'+menuNum+'"]').tab('show');
                   sessionStorage.removeItem("storeid");
                }
                else{}

            // + sign to list store List
            $scope.Storelist = function(){
                $scope.storeList = JSON.parse(json.data);
                //alert("data " + $scope.storeList);
                $('#selection-popup').modal('show');

                //--- dynamic theme Color for popup
                document.documentElement.style.setProperty('--main-bg-color', configDetailsProvider.apiConnect[0].color);
            }
          //check radio button
            $scope.selectRadio = function(value) {
               SelValue = value;
            };
            //Redirect to added store from store list
             $scope.storeFunc = function() {
                if (SelValue) {
                    var i = SelValue;
                    var num = "#menu"+i;
                     if(sessionStorage["siteID"] == i)
                       {
                         $scope["storearea" + i] = true;
                         $('a[data-target="'+num+'"]').tab('show');
                         $("ul.tab-nav-sec li a").ready(function() {
                             scrollDownFunc();
                         });
                       }
                       else
                       {
                          if(tabValue)
                          {
                            names.push(tabValue,i);

                           }
                          else
                          {
                            names.push(i);
                          }
                            var array1 = JSON.parse("[" + names + "]");
                            let uniqueNames = [...new Set(array1)];

                           localStorage["tab"] = uniqueNames;
                           $scope["storearea" + i] = true;
                           $('a[data-target="'+num+'"]').tab('show');

                $("ul.tab-nav-sec li a").ready(function() {
                    scrollDownFunc();
                });

                 }
          }
          }

            $scope.start = function() {

                $(".sidebar").toggleClass("open");

            }

            $scope.redirect = function(mode) {
                sessionStorage.setItem("key", mode);
                $window.location.href = "paymentApp/index.html";
            };

            var itemIndex = 0;
            $rootScope.msge = "";
            var messageCounter = 0;
            var msgh = "";
            $scope.messages = [];
            try {


                $scope.$watch(function() {
                    return $rootScope.messaRe;
                }, function(newVal, oldVal) {

                    if (newVal != undefined) {
                        $scope.messages = [];
                        msgh = JSON.parse(newVal);

                        for (var item = 0; item < msgh.length; item++) {

                            var msgHistory = {
                                data: msgh[item].data,
                                msg: msgh[item].msg,
                                index: msgh[item].index,
                                storeId: msgh[item].store,
                                time: msgh[item].time
                            };
                            $scope.messages.push(msgHistory);
                        };
                        scrollDownFunc();
                    }

                });

                $scope.getStoreId = function(store) {
                    return function(msg) {
                        return msg.storeId == store;
                    }
                }

                function scrollDownFunc() {
                    setTimeout(function() {

                        var $activeTab = $('.tab-content .tab-pane.active');
                        var activeId = $activeTab.attr('id');

                        var myid = $('#' + activeId).find('.chat-view-block').attr('id');

                        var elem = document.getElementById(myid);

                        elem.scrollTop = elem.scrollHeight;

                    }, 1000);
                }

                console.log('fjfgjfgfhj   ' + $scope.messages);

                $scope.send = function(event, number) {

                    var chatMsg = event.chat;
                    event.chat = null;

                    if (chatMsg != undefined) {

                        deviceReadyService.sendMessage(chatMsg, CreateDate(), number);

                    };
                }

                function CreateDate() {
                    var currentdate = new Date();
                    var currentDateTime = currentdate.toISOString();
                    return currentDateTime;
                };

            } catch (e) {
                console.log(e);
            }

        }]);