angular.module("app")
    .controller("aboutCtrl", ["$scope", "$rootScope", "$window", "$location", "configDetailsProvider", "SharedService", "$timeout", function($scope, $rootScope, $window, $location, configDetailsProvider, SharedService, $timeout) {

        $scope.clickToChat = function() {
            if (sessionStorage.getItem("siteID") == null || sessionStorage.getItem("siteID") == "") {
                $location.path("/errorMsg");
            } else {
                var siteID = sessionStorage.getItem("siteID");
                $location.path('/chat/' + siteID);
            }
        }

            var checkConfigData = function() {
            $timeout(function(){
            if(configDetailsProvider != undefined && configDetailsProvider.apiConnect.length>0){
              // theme color replaced by config color
               document.documentElement.style.setProperty('--main-bg-color', configDetailsProvider.apiConnect[0].color);
               $rootScope.IsShopShow = configDetailsProvider.apiConnect[0].IsShopShow;

                if (configDetailsProvider.apiConnect[0].flag == false) {
                    $(".ShopDisabled").css('pointer-events', 'none');
                    $(".ShopDisabled").css('opacity', '0.6');
                }

                $(".main-logo").css("background", configDetailsProvider.apiConnect[0].image);
                $(".main-logo").css("background-size", "contain");
            } else {
            checkConfigData();
            }

            },500)

            }
            checkConfigData();

        $scope.about = function() {
            $location.path("/about");
        };

       //Redirect Payment
       $scope.redirect = function(mode) {
        sessionStorage.setItem("key", mode);
        $window.location.href = "paymentApp/index.html";
       };

    }]);