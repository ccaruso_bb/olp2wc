﻿angular.module("app", ["ui.router", "ngCookies", "ngMessages", "sharedService"])
    .config(["$stateProvider", "$urlRouterProvider",
        function($stateProvider, $urlRouterProvider) {

            $stateProvider
                .state("start", {
                    url: "/start",
                    templateUrl: "views/start.html",
                    controller: "startCtrl",
                })
                .state("start.search", {
                    url: "/start.search",
                    templateUrl: "views/search.html",
                    controller: "searchCtrl",
                })
                .state("start.searchNotFound", {
                    url: "/start.searchNotFound",
                    templateUrl: "views/searchNotFound.html",
                    controller: "searchNotFoundCtrl",
                }).
            state("cartEmpty", {
                url: "/cartEmpty",
                templateUrl: "views/cartEmpty.html",
                controller: "cartEmptyCtrl",
            }).
            state("cartNotEmpty", {
                url: "/cartNotEmpty",
                templateUrl: "views/cartNotEmpty.html",
                controller: "cartNotEmptyCtrl",
            }).
            state("payDeposit", {
                url: "/payDeposit",
                templateUrl: "views/pay_deposit.html",
                controller: "payDepositCtrl",
            }).
            state("payDecline", {
                url: "/payDecline",
                templateUrl: "views/payment_decline.html",
                controller: "payDeclineCtrl",
            }).
            state("paySuccess", {
                url: "/paySuccess",
                templateUrl: "views/payment_success.html",
                controller: "paySuccessCtrl",
            });

            if (sessionStorage.getItem("siteID") == null || sessionStorage.getItem("siteID") == "") {

                $urlRouterProvider.otherwise("/errorMsg");
            } else {

                $urlRouterProvider.otherwise("/start");
            }

        }
    ])
    .run(function($rootScope, $window) { //Insert in the function definition the dependencies you need.
        //Do your $on in here, like this:
        $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams) {
            window.scrollTo(0, 0);
            if (sessionStorage.getItem("siteID") == null || sessionStorage.getItem("siteID") == "") {
                $rootScope.isVisibleTopMenu = false;
            } else {
                $rootScope.isVisibleTopMenu = true;
            }
        });
    });