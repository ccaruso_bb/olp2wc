﻿angular.module("app")
//    .controller("checkCtrl", ["$scope", "$rootScope", "idealsApi", "$location", "$locale","configDetailsProvider", function($scope, $rootScope, idealsApi, $location, $locale,configDetailsProvider) {
      .controller("checkCtrl", ["$scope", "$rootScope", "idealsApi", "$location", "$cookieStore", "$locale","configDetailsProvider", function($scope, $rootScope, idealsApi, $location, $cookieStore, $locale, configDetailsProvider) {

        var tokenError = '';

        try {

            $('#hamburger-inner-navbar').click(function () {
                $("#inner-navbar").toggleClass("in");
            });
            $("#isMsgEnable").show();
            $("#showMsg").hide();
            $rootScope.message = "";
            $scope.userFLName = sessionStorage.userFLName;
            var result = "";
            $scope.isSaving = false;
            $scope.cvvLength = 3;
            $scope.cardLength = 16;

            $scope.checkToken = "";
            $scope.currentYear = new Date().getFullYear();
            $scope.currentMonth = new Date().getMonth() + 1
//            $rootScope.apiErrorMessage="";
            var datetime = $locale.DATETIME_FORMATS; //get date and time formats
            var format = $locale['DATETIME_FORMATS']['medium'];
            $scope.months = datetime.SHORTMONTH; //access localized months
            console.log($scope.months);

//            var cookieWObject = ($cookieStore.get('payeezy_auth'));
              var cookieWObject = configDetailsProvider.apiConnect[0];
            //check selected payment amount and bind on checkout page html table
            if (sessionStorage["paymentAmt"] != "") {
                var count = 0;

                $scope.payment = JSON.parse(sessionStorage["paymentAmt"] || "{}");

                for (var item = 0; item < $scope.payment.length; item++) {
                    count = count + parseFloat($scope.payment[item].amountOfPayment);
                };

                $scope.totalAmount = (count).toFixed(2);
                $scope.total = "$" + (count).toFixed(2);

            };


         if (sessionStorage['cards'] != "")
         {
             $scope.items = JSON.parse(sessionStorage['cards'] || '{}');
         }

            if ($rootScope.disable) {
                $scope.isEnable1 = false;
                $scope.isEnable2 = true;
                $scope.isEnable3 = false;
            } else {

                if ($rootScope.checkProcessor) {
                 if ($rootScope.checkProcessor === 'payeezy') {
                     $scope.isEnable1 = true;
                     $scope.isEnable2 = false;
                     $scope.isEnable3 = false;
                 }
                 if ($rootScope.checkProcessor === 'cardconnect') {
                     $scope.isEnable1 = false;
                     $scope.isEnable2 = false;
                     $scope.isEnable3 = true;
//                    window.scrollTo(0, document.body.scrollHeight);
                    loadIframe();
                    function loadIframe(){
//                        document.getElementById("tokenframe").src = "https://ideal.cardconnect.com:" + (cookieWObject.env === 'development' ? '6443' : '8443') + "/itoke/ajax-tokenizer.html?css=%2Eerror%7Bcolor%3Ared%3Bborder-color%3Ared%3B%7Dbody%7Bmargin%3A0px%3B%7Dinput%7Bwidth%3A280px%3Bheight%3A22px%3Bpadding-left%3A10px%3Bpadding-right%3A10px%3Bpadding-top%3A1px%3Bpadding-bottom%3A1px%3B%7D&invalidinputevent=true";
//                        document.getElementById("tokenframe").src = "https://ideal.cardconnect.com:" + (cookieWObject.env === 'development' ? '6443' : '8443') + "/itoke/ajax-tokenizer.html?useexpiry=true&usecvv=true;css=%2Eerror%7Bcolor%3Ared%3Bborder-color%3Ared%3B%7Dbody%7Bmargin%3A0px%3B%7Dinput%7Bwidth%3A280px%3Bheight%3A22px%3Bpadding-left%3A10px%3Bpadding-right%3A10px%3Bpadding-top%3A1px%3Bpadding-bottom%3A1px%3B%7D&invalidinputevent=true";
                        document.getElementById("tokenframe").src = "https://ideal.cardconnect.com:" + (cookieWObject.env === 'development' ? '6443' : '8443') + "/itoke/ajax-tokenizer.html?useexpiry=true&usecvv=true&css=%2Eerror%7Bcolor%3Ared%3Bborder-color%3Ared%3B%7Dbody%7Bmargin%3A0px%3B%7Dinput%7Bborder%3A1px%20solid%20%23ccc%7D%23ccnumfield%7Bwidth%3A280px%3Bheight%3A28px%3Bpadding-left%3A10px%3Bmargin-top%3A10px%3Bpadding-top%3A1px%3Bpadding-bottom%3A1px%3B%7Dselect%7Bheight%3A28px%3Bmargin-top%3A10px%3Bpadding-left%3A10px%3Bmargin-right%3A10px%3B%7D%23cccvvfield%7Bwidth%3Aauto%3Bheight%3A28px%3Bmargin-top%3A10px%3Bpadding-left%3A10px%3B%7D&invalidinputevent=true";
                    }

                      var container = document.body;
                      element = document.getElementById('ccPay');
                      container.scrollTop = element.offsetTop;
                 }
             }
            }

            $scope.localDateTime = function(date) {
                var date = new Date(date);
                return date;
            }

            $scope.verifyCard = function(card_type) {

                $scope.cardholderName = "";
                $scope.cardNumber = "";
                $scope.exp_month = "";
                $scope.exp_year = "";
                $scope.cvvCode = "";
                $scope.zip_postal_code = "";

                $scope.paymentinfoform.cardholder_Name.$dirty = false;
                $scope.paymentinfoform.cardholder_Name.$pristine = true;

                $scope.paymentinfoform.card_Number.$dirty = false;
                $scope.paymentinfoform.card_Number.$pristine = true;

                $scope.paymentinfoform.cvv_code.$dirty = false;
                $scope.paymentinfoform.cvv_code.$pristine = true;

                $scope.paymentinfoform.zip_postal_code.$dirty = false;
                $scope.paymentinfoform.zip_postal_code.$pristine = true;

                $scope.paymentinfoform.$submitted = false;

                if (card_type == "American Express") {
                    $scope.cardLength = 15;
                    $scope.cvvLength = 4;
                } else {
                    $scope.cardLength = 16;
                    $scope.cvvLength = 3;
                }
            }

            $scope.ccVerifyCard = function(card_type){

                $scope.cardholderName = "";
//                $scope.ccCardNumber = "";
                $scope.exp_month = "";
                $scope.exp_year = "";
                $scope.cvvCode = "";
                $scope.zip_postal_code = "";

                $scope.cc_paymentinfoform.cardholderName.$dirty = false;
                $scope.cc_paymentinfoform.cardholderName.$pristine = true;


//                $scope.cc_paymentinfoform.exp_month.$dirty = false;
//                $scope.cc_paymentinfoform.exp_month.$pristine = true;
//
//                $scope.cc_paymentinfoform.exp_year.$dirty = false;
//                $scope.cc_paymentinfoform.exp_year.$pristine = true;
//
//                $scope.cc_paymentinfoform.cvvCode.$dirty = false;
//                $scope.cc_paymentinfoform.cvvCode.$pristine = true;

                $scope.cc_paymentinfoform.zip_postal_code.$dirty = false;
                $scope.cc_paymentinfoform.zip_postal_code.$pristine = true;

                $scope.cc_paymentinfoform.$submitted = false;

                if (card_type == "American Express") {
                    $scope.cardLength = 15;
                    $scope.cvvLength = 4;
                } else {
                    $scope.cardLength = 16;
                    $scope.cvvLength = 3;
                }
            }

        //Card types
         $scope.cardTypes = [
             "Visa",
             "Master Card",
             "American Express",
             "Discover"
         ]


//            $scope.MakePayment_NC = function() {
            $scope.MakePayment_Payeezy = function() {
                var myNumber = $scope.exp_month;
                var formattedNumber = ("0" + myNumber).slice(-2);
                $scope.exp_month = formattedNumber;

                $("#hideMsg").hide();
                $("#showMsg").show();
                var $form = $("#paymentinfoform");
                $("#someHiddenDiv").hide();
                if ($scope.paymentinfoform.$valid) {
                    $rootScope.ShowLoader = true;

               //..........android and ios back button diable ..................
                document.addEventListener("deviceready", onDeviceReady, false);
                function onDeviceReady() {
                    document.addEventListener("backbutton", function(e) {
                        e.preventDefault();
                    }, false);
                }
                //..........ended...........

                    $scope.isSaving = true;
                    $form.find("#paymentinfo").prop("disabled", true);
                    var apiData= configDetailsProvider.apiConnect[0];
                   // SharedService.writePayezzyCookie();
                   // var cookieWObject = apiData.payeezy_auth;
                    var apiKey = apiData.api_key;
                    var js_security_key = apiData.payeezy_key;
                    var ta_token = apiData.ta_token;
//                    var auth = true;
                    var payeezy_auth = true;
                    Payeezy.setApiKey(apiKey);
                    Payeezy.setJs_Security_Key(js_security_key);
                    Payeezy.setTa_token(ta_token);
//                    Payeezy.setAuth(auth);
                    Payeezy.setAuth(payeezy_auth);
                    Payeezy.createToken(responseHandler, apiData.env);
                    $("#someHiddenDiv").show();
                    return false;
                }
            };

           //card Connect start
          $scope.cc_NewCardPayment = function () {

             if ($scope.cc_paymentinfoform.$valid) {
                 $rootScope.ShowLoader = true;
                 var cvvCode = $scope.cvvCode;
                 var expYear = $scope.exp_year;
//                 var expDate = $scope.exp_month;
                 var expMonth = $scope.exp_month;
//                 var ccCardNumber = $scope.ccCardNumber;
                 var Cardtoken = document.getElementById('token').value;
                 var cardholderName = $scope.cardholderName;
                 var cardType = $scope.cardType;
                 var Cardtoken =  document.getElementById('token').value;
                 var cardTokenLength = Cardtoken.length;
                 var first2 = Cardtoken.substr(1, 2);
                 var first6 = first2 + "****";
                 var last4 = Cardtoken.substr((cardTokenLength-4), 4);

                 var processType = "entry";
                 var zipCode = $scope.zip_postal_code;

//                 var expDate = expMonth + expYear;
                 var expDate = document.getElementById('expDate').value;
                 console.log('-------expDate---------:' + expDate)
//                 var first6 = ccCardNumber.substr(0, 6);
//                 var last4 =  ccCardNumber.substr(12, 4);
                 var data = idealsApi.pay_stored_cardReq(Cardtoken, expDate, first6, last4, cardholderName, cardType, $scope.payment, $scope.totalAmount, processType, zipCode);
             }
         }

          window.addEventListener('message', function (event) {
              console.log("received message: " + event.data);
              //$scope.checkToken = "";
              var token = JSON.parse(event.data);
              document.getElementById("cardNumberValidationError").innerHTML = '';
              if (token.validationError === undefined && ($scope.cc_paymentinfoform.$valid == true)) {
                 document.getElementById("ccNCpayment").disabled = false;
                 document.getElementById("cardNumberValidationError").innerHTML = '';
              }
              else if(token.message == "")
              {
                  document.getElementById("ccNCpayment").disabled = true;
                  document.getElementById("cardNumberValidationError").innerHTML = 'Invalid details CVV or Exp or CC-Number';
//                    document.getElementById("cardNumberValidationError").innerHTML = token.validationError;
                  //$scope.cardNumberValidationError = 'Card is invalid';
              }
              tokenError = token.validationError;
              $scope.checkToken = token.message;
              document.getElementById('token').value = token.message;
              document.getElementById('expDate').value = token.expiry;
              console.log("token.message ------- token:" + JSON.stringify(token));
          }, false);

          //Card Connect end

             $scope.NonSavedCard = function () {

                 $rootScope.disable = false;

                 if (cookieWObject.processor_type == 0) {
                     $rootScope.apiErrorMessage = "";
                     $('#payment-errors').html("");
                     $('#paymentinfoform').trigger("reset");
                     $scope.paymentinfoform.cardholder_Name.$dirty = false;
                     $scope.paymentinfoform.cardholder_Name.$pristine = true;

                     $scope.paymentinfoform.card_Number.$dirty = false;
                     $scope.paymentinfoform.card_Number.$pristine = true;

                     $scope.paymentinfoform.cvv_code.$dirty = false;
                     $scope.paymentinfoform.cvv_code.$pristine = true;

                     $scope.paymentinfoform.zip_postal_code.$dirty = false;
                     $scope.paymentinfoform.zip_postal_code.$pristine = true;

                     $scope.paymentinfoform.$submitted = false;

                     $rootScope.message = "";
                     $('#hideMsg').show();
                     $('#showMsg').hide();
                     $scope.isEnable1 = true;
                     $scope.isEnable2 = false;
                     $scope.isEnable3 = false;

                 }

                 else if (cookieWObject.processor_type == 1) {
                     loadIframe();
                     function loadIframe(){
                             document.getElementById("tokenframe").src = "https://ideal.cardconnect.com:" + (cookieWObject.env === 'development' ? '6443' : '8443') + "/itoke/ajax-tokenizer.html?useexpiry=true&usecvv=true&css=%2Eerror%7Bcolor%3Ared%3Bborder-color%3Ared%3B%7Dbody%7Bmargin%3A0px%3B%7Dinput%7Bborder%3A1px%20solid%20%23ccc%7D%23ccnumfield%7Bwidth%3A280px%3Bheight%3A28px%3Bpadding-left%3A10px%3Bmargin-top%3A10px%3Bpadding-top%3A1px%3Bpadding-bottom%3A1px%3B%7Dselect%7Bheight%3A28px%3Bmargin-top%3A10px%3Bpadding-left%3A10px%3Bmargin-right%3A10px%3B%7D%23cccvvfield%7Bwidth%3Aauto%3Bheight%3A28px%3Bmargin-top%3A10px%3Bpadding-left%3A10px%3B%7D&invalidinputevent=true";
                     }
                     $rootScope.apiErrorMessage = "";
                     $('#payment-errors').html("");
                     $('#cc_paymentinfoform').trigger("reset");

                    $scope.cc_paymentinfoform.cardholderName.$dirty = false;
                    $scope.cc_paymentinfoform.cardholderName.$pristine = true;

    //                $scope.paymentinfoform.card_Number.$dirty = false;
    //                $scope.paymentinfoform.card_Number.$pristine = true;

                    $scope.cc_paymentinfoform.exp_month.$dirty = false;
                    $scope.cc_paymentinfoform.exp_month.$pristine = true;

                    $scope.cc_paymentinfoform.exp_year.$dirty = false;
                    $scope.cc_paymentinfoform.exp_year.$pristine = true;

                    $scope.cc_paymentinfoform.cvvCode.$dirty = false;
                    $scope.cc_paymentinfoform.cvvCode.$pristine = true;

                    $scope.cc_paymentinfoform.zip_postal_code.$dirty = false;
                    $scope.cc_paymentinfoform.zip_postal_code.$pristine = true;

                     document.getElementById("cardNumberValidationError").innerHTML = '';


                     $scope.cc_paymentinfoform.$submitted = false;

                     $rootScope.message = "";
                     $('#hideMsg').show();
                     $('#showMsg').hide();
                     $scope.isEnable1 = false;
                     $scope.isEnable2 = false;
                     $scope.isEnable3 = true;

                 }
                 else {
                     return false;
                 }
             }


            $scope.savedCard = function() {
                $rootScope.apiErrorMessage = "";
                $("#payment-errors").html("");
                $("#paymentinfo").trigger("reset");

                $scope.paymentinfoform.$submitted = false;

                $rootScope.message = "";
                $("#hideMsg").show();
                $("#showMsg").hide();
                $scope.isEnable1 = false;
                $scope.isEnable3 = false;
                $scope.isEnable2 = true;
            }

            $scope.MakePayment_SC = function(Selectedcard) {
                $("#hideMsg").hide();
                $("#showMsg").show();
                if ($scope.paymentinfo.$valid) {
                    $rootScope.ShowLoader = true;
                    var $form = $("#paymentinfo");
                    for (var item = 0; item < $scope.items.length; item++) {
                        var cardinfo = Selectedcard;
                        if ($scope.items[item].last4 == cardinfo.last4) {
                            var Cardtoken = $scope.items[item].cardToken;
                            var expDate = ($scope.items[item].expDate);
                            var first6 = $scope.items[item].first6;
                            var last4 = $scope.items[item].last4;
                            var cardType = $scope.items[item].cardType;
                            var cardHolder = $scope.items[item].cardHolder;
                            var zip = $scope.items[item].zip; //added as suggested by Rick@ 06-13-19 Firoj
                            var processType = "stored";
//                            var processType = "entry";
                            break;
                        }
                    }
                    $form.find("#paymentinfo").prop("disabled", true);
                    idealsApi.pay_stored_cardReq(Cardtoken, expDate, first6, last4, cardHolder, cardType, $scope.payment, $scope.totalAmount, processType, zip);
                }
            };

            var responseHandler = function(status, response) {

                var $form = $("#paymentinfoform");
                if (status != 201) {
                    if (response.Error && status != 400) {
                        var error = response["Error"];
                        var errormsg = error["messages"];
                        var errorcode = JSON.stringify(errormsg[0].code, null, 4);
                        var errorMessages = JSON.stringify(errormsg[0].description, null, 4);
                        $("#payment-errors").html("Error Messages: " + errorMessages);
                    }
                    if (status == 400 || status == 500) {
                        if (status == 500) {
                            var errorMessages = "";
                            var errormsg = response.error;
                            errorMessages = errorMessages + "Error Messages:" +
                                errormsg;
                        } else {
                            var errormsg = response.Error.messages;
                            var errorMessages = "";
                            for (var i in errormsg) {
                                var ecode = errormsg[i].code;
                                var eMessage = errormsg[i].description;
                                errorMessages = errorMessages + "Error Code:" + ecode + ", Error Messages:" +
                                    eMessage;
                            }
                        }
                        $("#payment-errors").html(errorMessages);
                    }
                    $("#hideMsg").show();
                    $("#showMsg").hide();
                    $form.find("#NCpayment").prop("disabled", false);
                    $("#imageDiv").hide();
                    $form.find("imageDiv").prop("disabled", true)
                } else {
                    $("#someHiddenDiv").hide();
                    result = response.token.value;
                    $form.find("#NCpayment").prop("disabled", true);
                    var cvvCode = $scope.cvvCode;
                    var expYear = $scope.exp_year;
                    var expDate = $scope.exp_month;
                    var cardNumber = $scope.cardNumber;
                    var cardholderName = $scope.cardholderName;
//                    var taTokan = $scope.taTokan;
//                    var authCheck = $scope.authCheck;
                    var cardType = $scope.cardType;
                    var Cardtoken = result;
                    var processType = "entry";
                    $form.find("button").prop("disabled", false);
                    var expDate = expDate + expYear;
                    var first6 = cardNumber.substr(0, 6);
                    var last4 = cardNumber.substr(12, 4);
                    var zip = $scope.zip_postal_code;
//                    idealsApi.pay_stored_cardReq(Cardtoken, expDate, first6, last4, cardholderName, cardType, $scope.payment, $scope.totalAmount, processType);
                    idealsApi.pay_stored_cardReq(Cardtoken, expDate, first6, last4, cardholderName, cardType, $scope.payment, $scope.totalAmount, processType, zip);
                }
                $rootScope.ShowLoader = false;

                //...........android and ios back button diable ...............
                document.addEventListener("deviceready", onDeviceReady, false);

                function onDeviceReady() {
                    document.addEventListener("backbutton", function(e) {
                        e.preventDefault();
                    }, true);
                }
                //............ended................

            };

            $scope.paymentHistory = function() {
                idealsApi.payment_historyReq();
            }

        } catch (e) {
            $form.find("imageDiv").prop("disabled", true)

            //.........android and ios back button diable ...............
            document.addEventListener("deviceready", onDeviceReady, false);

            function onDeviceReady() {
                document.addEventListener("backbutton", function(e) {
                    e.preventDefault();
                }, false);
            }
            //.............ended.............

            console.log(e);
        }
    }]);