﻿angular.module("app")
    .controller("errorCtrl", ["$scope", "$rootScope", "idealsApi", "$window", "$location", function($scope, $rootScope, idealsApi, $window, $location) {

        try {
            $scope.ErrorMessage = $location.search().ref;
            $scope.refresh = function() {
                $window.location.reload();
            };
        } catch (e) {
            console.log(e);
        }
    }]);